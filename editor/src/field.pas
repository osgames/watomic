unit field;
{
------------------------------------------------------------------
WATOMIC - Windows(c) KAtomic clone

          PlayField and atoms implementation unit;

Copyright (C) 2004-2005  Federico Blaseotto (ironfede)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
------------------------------------------------------------------
}

//GLTexture: line 3996 produces an error on CyberTrident; there is a problem somewhere.

interface

uses Classes, GLScene,{ GLUtils,} GLObjects,
     GlTexture, GLCadencer, IniFiles, SysUtils, Graphics, Types;


const
  CELL_LENGTH = 1/16;
  V_CELL_LENGTH = CELL_LENGTH;

type
  TMovement = (mUp,mLeft,mRight,mDown);

//***************************************************************************
  TDestination = record
    row:integer;
    col:integer;
    end;

//***************************************************************************
//Textual field representation
  TPlayGrid = array [0..14,0..14] of char;
  

//***************************************************************************
//Atom on glscene class; it implements graphic and movement behaviour.
  TWAtom = class(TGLSprite)
    private
      FSpec: string;     //specie chimica e legame
      FAtomID: string;   //ID dell'atomo
      FRow: integer;
      FCol: integer;
      FMovType: TMovement;
      FDestination: TDestination;
      FIsMoving: boolean;
      FNextAtom: TWAtom;

      procedure SetSpec(const Value: string);
      procedure SetAtomID(const Value: string);
      procedure SetCol(const Value: integer);
      procedure SetRow(const Value: integer);
      procedure SetMovType(const Value: TMovement);
      procedure SetIsMoving(const Value: boolean);
      procedure SetNextAtom(const Value: TWAtom);

      property Col:integer  read FCol write SetCol;
      property Row:integer  read FRow write SetRow;
      property MovType:TMovement read FMovType write SetMovType;

    public
      constructor Create(AOwner:TComponent);override;
      destructor Destroy;override;

   (**********************************************)
      //this properties must be public
      property Spec:string read FSpec write SetSpec;
      property AtomID:string read FAtomId write SetAtomID;
   (**********************************************)

      property IsMoving:boolean read FIsMoving write SetIsMoving;
      property NextAtom:TWAtom read FNextAtom write SetNextAtom;

    end;

//***************************************************************************
  TPlayField = class(TGLScene)
    private
      PlayGrid:TPlayGrid;
      LevelFile: TMemIniFile;

      FLevel: string;
      FSelected: TWatom;
      FMolek_Name: string;
      FMoves: integer;
      FOnLevelCompleted: TNotifyEvent;
      FLevelNumber: integer;
      FWallBitmap:TBitmap;
      FSpeed: integer;

   (**********************************************)
      FNoCompletion: Boolean;
      FSingleMovement: Boolean;
      FAddWallsAsAtom: Boolean;
      FFirstAtom: TWAtom;

      procedure AddAtomToList(Atom: TWAtom);
      procedure DeleteAtomFromList(Atom: TWAtom);
      procedure DoAddWall(Row, Col: Integer; AddAsAtom: Boolean);
      function GetCells(Row, Col: Integer): Char;
   (**********************************************)

      procedure SetLevel(const Value: string);
      procedure SetSelected(const Value: TWatom);
      procedure SetMolek_Name(const Value: string);
      procedure SetMoves(const Value: integer);
      procedure SetOnLevelCompleted(const Value: TNotifyEvent);

    protected
      procedure DoLevelCompleted;dynamic;
      procedure DoCadProgress(Sender: TObject; const deltaTime,newTime: Double);dynamic;

    public
   (**********************************************)
      GoalViewer:TPlayField;
   (**********************************************)
      Cd:TGLCadencer;

      constructor Create(AOwner:TComponent);override;
      destructor Destroy;override;

      property Molek_Name:string read FMolek_Name write SetMolek_Name;
      property Level:string read FLevel write SetLevel;
      property Selected:TWatom read FSelected write SetSelected;
      property Moves:integer read FMoves write SetMoves;
      property LevelNumber:integer read FLevelNumber;
      property OnLevelCompleted:TNotifyEvent read FOnLevelCompleted write SetOnLevelCompleted;
      property Speed:integer read FSpeed write FSpeed;

   (**********************************************)
      property NoCompletion: Boolean read FNoCompletion write FNoCompletion;
      property SingleMovement: Boolean read FSingleMovement write FSingleMovement;
      property AddWallsAsAtom: Boolean read FAddWallsAsAtom write FAddWallsAsAtom;
      property FirstAtom: TWAtom read FFirstAtom;
      property Cells[Row, Col: Integer]: Char read GetCells;

      procedure AddAtom(ARow, ACol: Integer; AAtomID: Char; ASpec: String);
      procedure DeleteAtom(Atom: TWAtom);
      procedure AddWall(Row, Col: Integer);
   (**********************************************)

      function IsCompleted:boolean;
      procedure MoveAtomLeft;
      procedure MoveAtomRight;
      procedure MoveAtomUp;
      procedure MoveAtomDown;

      procedure SaveLevel(AFileName:string);
      procedure LoadLevel(AFileName:string);

      procedure ApplyMover;
      procedure RemoveMover;
      procedure ClearField;

    end;


implementation

//images.res --HAS-- to be compiled:
//it contains all atoms and binds images
//you can use the "compile_res.bat" in 'src' dir.
{$R images.res}

uses Dialogs;

//------------------------------------------------------------------------------
{ TPlayField }

constructor TPlayField.Create(AOwner: TComponent);
var
  i, j: Integer;
begin
  inherited;
 (**********************************************)
    //this lines are necessary.
    Speed := 5;
    
    for  i:= 0 to 14 do
      for j := 0 to 14 do
      begin
        PlayGrid[i,j]:='.';
      end;
 (**********************************************)
  Cd:=TGLCadencer.Create(Self);
  Cd.Enabled:=FALSE;
  Cd.Scene:=Self;
  //Cd.TimeMultiplier:=-1;
  Cd.OnProgress:=DoCadProgress;

  FWallBitmap:=TBitmap.Create;
  FWallBitmap.Height:=32;
  FWallBitmap.Transparent:=FALSE;
  FWallBitmap.Width:=32;
  FWallBitmap.LoadFromResourceName(hinstance,'WALL');

end;

//------------------------------------------------------------------------------
destructor TPlayField.Destroy;
begin
  FreeAndNil(LevelFile);
  FreeAndNil(Cd);
  FreeAndNil(FWallBitmap);

  inherited;
end;

//------------------------------------------------------------------------------
//Here the atom's movements are implemented through cadencer
procedure TPlayField.DoCadProgress(Sender: TObject; const deltaTime,
  newTime: Double);
var
  STEP:Double;

const
  Speed1X = 0.08;
  Speed10X = 1.5;

begin
  STEP := (Speed1X + (FSpeed - 1) * (Speed10X - Speed1X) / 9) * deltaTime;

  //The following stuff is organized in order to avoid
  //positioning errors due approximation in floating point
  //operations.

  Case Selected.MovType of

  mLeft:
    if  STEP<-(-7*CELL_LENGTH+CELL_LENGTH*Selected.FDestination.col) + Selected.Position.X then
        begin
        //posX:= posX-0.1;
        Selected.Position.X := Selected.Position.X - STEP;
        exit;
        //ot:=Cad.GetCurrentTime;  Cad.CurrentTime-ot
        end;

  mRight:
      if (STEP) < (-7*CELL_LENGTH+CELL_LENGTH*Selected.FDestination.col) - Selected.Position.X then

        begin
        Selected.Position.X :=Selected.Position.X + STEP;
        exit;
        end;

  mUp:
      if -Selected.Position.Y + (7*CELL_LENGTH - CELL_LENGTH*Selected.FDestination.row) > STEP then
        begin
        Selected.Position.Y :=Selected.Position.Y +STEP;;
        exit;
        end;

  mDown:
      if (Selected.Position.Y -(7*CELL_LENGTH - CELL_LENGTH*Selected.FDestination.row) > STEP ) then
        begin
        Selected.Position.Y :=Selected.Position.Y - STEP;
        exit;
        end;
  end; //case

  //(((((((((((((((((((((((((((((   (((((((((((((((((  ((((((((((  (((((((
  //If execution flow reaches this point it means the atom take the right place
  //It is -correctly- positioned in its last movement
  Selected.Position.X:=-7*CELL_LENGTH+CELL_LENGTH*Selected.FDestination.col;
  Selected.Position.Y:=(7*CELL_LENGTH) - CELL_LENGTH*Selected.FDestination.row;

  Selected.IsMoving:=FALSE;
  cd.Enabled:=FALSE; //...we don't want CPU 100%

  if not IsCompleted then
    Applymover;
end;

//------------------------------------------------------------------------------
procedure TPlayField.SetLevel(const Value: string);
var
(**********************************************)
  Line:string;
  i,j:integer;
  //ps,wl: TGLBaseSceneObject;      //Added object
  //tempAtom:TWAtom;
  //firstAtom:TWAtom;

  NewSpec: String;
(**********************************************)
begin
  ClearField;
  FLevel := Value;

  if not FileExists(FLevel) then
  begin
    MessageDlg('Level file not found'+#13#10+'Check your ''levels'' dir path !',mtError,[mbOk],0);
    Exit;
  end;

  LevelFile:=TMemIniFile.Create(value);
  Molek_Name:=LevelFile.ReadString('Level','Name',' ');

(**********************************************)
  {ps:=nil;
  FirstAtom:=nil;//pointer to first atom created}
(**********************************************)

  for i := 0 to 14 do
  begin
    if i<10 then
      Line:=LevelFile.ReadString('Level','feld_'+ IntToHex((i),2),'NaS')
    else
      Line:=LevelFile.ReadString('Level','feld_'+ IntToStr(i),'NaS');
    //raise if NaS

    for  j:= 0 to 14 do
    begin
  (**********************************************)
      if ((Line[j+1] in ['0'..'z'])or((Line[j+1]='#')and(FAddWallsAsAtom)))then
      begin
        NewSpec := '';
        if Line[j+1] <> '#' then
          NewSpec:=LevelFile.ReadString('Level','atom_'+ Line[j+1],'NaS');
        AddAtom(i, j, Line[j+1], NewSpec);

        (*//---here is an implementation of
        //a 'on the fly' circular linked list
        //so that every atom has a ponter to the next
        //and the last one refers to the first one.

        tempAtom:=TWatom(ps); //ps initially nil
        ps:= TWatom(Objects.AddNewChild(TWAtom)); //Create atom
        //TWAtom(ps).PF:=Self;

        if tempAtom <> nil then
          tempAtom.NextAtom:=TWAtom(ps)
        else  //first atom just created
          FirstAtom:=TWAtom(ps);
        //---

        with TWatom(ps) do
        begin
          AtomID:=Line[j+1];             //Set the Atom ID
          PlayGrid[j,i]:=Line[j+1];      //Refresh text grid   [0..x]

          //Now we set the chemical aspect of the atom
          //through a string formatted as follow:
          //<Chemical>-[<bind1><bind2>...<bindn>]
          Spec:=LevelFile.ReadString('Level','atom_'+ Line[j+1],'NaS')

          Row:=i;
          Col:=j;
          Position.X:=-CELL_LENGTH*7 {- CELL_LENGTH/2}+CELL_LENGTH*(j);
          Position.Y:=CELL_LENGTH*7 {+ CELL_LENGTH/2}-CELL_LENGTH*(i);
          Position.Z:=0;

          Visible:=TRUE;
        end;*)
      end
      else
        if ( (Line[j+1] = '#') ) then  //Wall
        //with TGLSprite(wl) do
        begin
          DoAddWall(i, j, False);
          (*wl:= TGLSprite(Objects.AddNewChild(TGLSprite));

          TGLSprite(wl).Material.Texture:=TGLTexture(FWallBitmap);
          TGLSprite(wl).Material.Texture.Disabled:=FALSE;

          PlayGrid[j,i]:=Line[j+1] ;     //Refresh Array Grid  (#)

          Position.X:=-CELL_LENGTH*7 {- CELL_LENGTH/2}+CELL_LENGTH*(j);
          Position.Y:=CELL_LENGTH*7 {+ CELL_LENGTH/2}-CELL_LENGTH*(i);
          Position.Z:=0;

          Height:=CELL_LENGTH;
          Width:=CELL_LENGTH;

          Visible:=TRUE;

          Position.z:=0;*)
        end
        else
          PlayGrid[j,i]:=Line[j+1];      //Refresh Array Grid (.)
  (**********************************************)
    end;
  end;

(**********************************************)
  //closed linked list;
  //[LastAtom].nextatom = FirstAtom
  {if ps <> nil then
    TWatom(ps).NextAtom:=FirstAtom;
  Self.FFirstAtom := firstAtom;}
(**********************************************)

  i:=0;
  j:=0;
  Line:=LevelFile.ReadString('Level','mole_'+ IntToStr(i),'NaS');

  if GoalViewer<>nil then
  begin
    while Line<>'NaS' do
    begin
      while j<=(Length(Line)-1) do
      begin
        if ( (Line[j+1] in ['0'..'z']) ) then
        begin
     (**********************************************)
          NewSpec:=LevelFile.ReadString('Level','atom_'+ Line[j+1],'NaS');
          GoalViewer.AddAtom(i, j, Line[j+1], NewSpec);
          
          (*tempAtom:=TWatom(ps); //ps initially nil
          ps:= TWatom(GoalViewer.Objects.AddNewChild(TWAtom));

          TWAtom(ps).Height:=V_CELL_LENGTH;
          TWAtom(ps).Width:=V_CELL_LENGTH;
          with TWatom(ps) do
          begin
            AtomID:=Line[j+1];  //Set Atom ID
            // Send identificative string to  Atom
            Spec:=LevelFile.ReadString('Level','atom_'+ Line[j+1],'NaS');

            Position.X:=-V_CELL_LENGTH*7{- V_CELL_LENGTH/2}+V_CELL_LENGTH*(j);
            Position.Y:=V_CELL_LENGTH*7{+ V_CELL_LENGTH/2}-V_CELL_LENGTH*(i);
            Position.Z:=0;
            Visible:=TRUE;
          end;*)
     (**********************************************)
        end;
        j:=j+1;
      end;
      j:=0;
      i:=i+1;
      Line:=LevelFile.ReadString('Level','mole_'+ IntToStr(i),'NaS');
    end;
  end;

  if not LevelFile.SectionExists('SAVED') then
    begin
    //Standard level file, Level number has to be extracted
    //from filename;
 (**********************************************)
    //This cause errors with loading levels in level editor.
    //FLevelNumber:=StrToInt(Copy(ExtractFileName(FLevel),7,3)) ;
    FMoves:=0;
    FLevelNumber:=StrToIntDef(Copy(ExtractFileName(FLevel),7,3), 1) ;
 (**********************************************)
    end
  else
    //Custom saved level file: we don't have a "standard filename"
    //so we extract the number of level from an early saved
    //field.
    begin
    FLevelNumber:=LevelFile.ReadInteger('SAVED','levelnumber',1);
    FMoves:= LevelFile.ReadInteger('SAVED','moves',0);
    end;
end;


//------------------------------------------------------------------------------
procedure TPlayField.MoveAtomLeft;  //See MoveAtomRight for English translation
var
  tomove:integer;//contiene l'indice della colonna di arrivo

begin
  if (not(Assigned(Selected))) then exit;

  if Selected.IsMoving then Exit;

  tomove:=Selected.Col;//il movimento parte dalla Col. selezionata, verso sx

  repeat
    tomove:=tomove-1; //sposta di una casella a sx...
  until
    not(PlayGrid[tomove,Selected.Row] = '.')
    or (tomove=-1); //fino al muro o atomo

  tomove:=tomove+1; //quando esce dal ciclo ha gi� letto una casella bloccata

  if tomove=Selected.Col then exit; //Nessuna possibilit� di movim.

(**********************************************)
  if FSingleMovement then
    if tomove < Selected.Col - 1 then
      tomove := Selected.Col - 1;
(**********************************************)

  PlayGrid[Selected.Col,Selected.Row]:='.' ;  //la pos. di partenza si svuota...
  PlayGrid[tomove,Selected.Row]:=(Selected.AtomID[1]);//quella di arrivo viene
                                                      //riempita dall'ID

  Selected.Col:=tomove; //cambio l'attributo Col dell'atomo
  Selected.FDestination.row:=Selected.FRow;
  Selected.FDestination.col:=tomove;//indico la casella di arrivo  per CAD...
  Selected.MovType:=mLeft;//... e gli dico che movimento attuare...
  Selected.IsMoving:=TRUE;
  RemoveMover;
  Moves:=Moves+1;
  cd.Enabled:=TRUE;
end;

//------------------------------------------------------------------------------
procedure TPlayField.MoveAtomDown;
var
  tomove:integer;

begin
  if (not(Assigned(Selected))) then exit;

  if Selected.IsMoving then Exit;

  tomove:=Selected.Row;

  repeat
    tomove:=tomove+1;
  until
    not(PlayGrid[Selected.col,tomove] = '.')
    or (tomove=15);
    //There is the possibility for no border wall

  tomove:=tomove-1; //quando esce dal ciclo ha gi� letto

  if tomove=Selected.Row then exit; //Nessuna possibilit� di movim.

(**********************************************)
  if FSingleMovement then
    if tomove > Selected.Row + 1 then
      tomove := Selected.Row + 1;
(**********************************************)

  PlayGrid[Selected.Col,Selected.Row]:='.' ;
  PlayGrid[Selected.Col,tomove]:=(Selected.AtomID[1]);

  Selected.row:=tomove;
  Selected.FDestination.row:=tomove;
  Selected.FDestination.col:=Selected.FCol;
  Selected.MovType:=mDown;
  Selected.IsMoving:=TRUE;
  RemoveMover;
  Moves:=Moves+1;
  cd.Enabled:=TRUE;
end;

//------------------------------------------------------------------------------
procedure TPlayField.MoveAtomRight;
var
  tomove:integer;//Destination column index

begin
  if (not(Assigned(Selected))) then exit; //Check for selection

  if Selected.IsMoving then Exit;  //Check for running atom

  tomove:=Selected.Col;

  //Increase the column index until
  //there is possibility for movement
  repeat
    tomove:=tomove+1;
  until
    not(PlayGrid[tomove,Selected.Row] = '.')
    or (tomove=15);

   tomove:=tomove-1 ;  //wall/Atom already readed: count back

  if tomove=Selected.Col then exit; //No movement possibility

(**********************************************)
  if FSingleMovement then
    if tomove > Selected.Col + 1 then
      tomove := Selected.Col + 1;
(**********************************************)
    
  //Start Text grid position empty
  PlayGrid[Selected.Col,Selected.Row]:='.' ;

  //Final Text grid position setted with selected ATOmId
  PlayGrid[tomove,Selected.Row]:=(Selected.AtomID[1]);

  Selected.Col:=tomove;//Refresh Atom-column-index
  Selected.FDestination.row:=Selected.FRow;
  Selected.FDestination.col:=tomove;
  Selected.MovType:=mRight;
  Selected.IsMoving:=TRUE;
  RemoveMover;   //Remove movement arrows from ready-to-go atom
  Moves:=Moves+1;
  cd.Enabled:=TRUE;  //Start the cadencer
end;


//------------------------------------------------------------------------------
procedure TPlayField.MoveAtomUp;
var
  tomove:integer;

begin
  if (not(Assigned(Selected))) then exit;

  if Selected.IsMoving then Exit;

  tomove:=Selected.Row;

  repeat
    tomove:=tomove-1;
  until
    not(PlayGrid[Selected.col,tomove] = '.')
    or (tomove=-1);

  tomove:=tomove+1 ;

  if tomove=Selected.Row then exit; //Nessuna possibilit� di movim.

(**********************************************)
  if FSingleMovement then
    if tomove < Selected.Row - 1 then
      tomove := Selected.Row - 1;
(**********************************************)
    
  PlayGrid[Selected.Col,Selected.Row]:='.' ;
  PlayGrid[Selected.col,tomove]:=(Selected.AtomID[1]);

  Selected.Row:=tomove;
  Selected.FDestination.row:=tomove;
  Selected.FDestination.col:=Selected.FCol;
  Selected.MovType:=mUp;
  Selected.IsMoving:=TRUE;
  RemoveMover;
  Moves:=Moves+1;
  cd.Enabled:=TRUE;
end;

//------------------------------------------------------------------------------
//Check for completed level: TRUE if completed, FALSE otherwise
function TPlayField.IsCompleted: boolean;
var
  goal_lines: array[0..14] of string;
  hL,vL,i,j,im,jm:integer;
  c:char;

begin
(**********************************************)
  if FNoCompletion then
  begin
    Result := False;
    Exit;
  end;
(**********************************************)
  
  for i:=0 to 14 do
    goal_lines[i]:='';

  //Trying to explain...
  //We create a sub-matrix of chars
  //(dimensions hL x vL)
  //containing the target-molecule...
  i:=0;
  repeat
    goal_lines[i]:=LevelFile.ReadString('Level','mole_'+IntToStr(i),'NaS');
    i:=i+1;
  until
    (goal_lines[i-1]='NaS');

  //Vertical lenght of char matrix representing target molecular form
  //0-based
  vL:=i-1;

  //Horizontal lenght of char matrix representing target molecular form
  //1-based
  hL:=Length(goal_lines[0]);

  result:=FALSE;

  //...afterthat we move this
  //submatrix around the text-grid
  //looking for match.
  //While matching we have to
  //ignore the presence of
  //intermediate wall
  //so we use an extra temp var 'c'
  //Matching is done
  //line by line on the submatrix
  //and when there is no
  // match Result is false setted.

  for i := 0 to 15-vL do
  begin
    for j := 0 to 15-hL do
    begin
      result := TRUE;
      for  im:= 0  to vL-1 do
        begin
        for  jm:= 1 to hL do
          begin
          c:=PlayGrid[j+jm-1,i+im];
          //A simple consideration:
            //The min x-value of playgrid
            //is 0+1-1 = 0 OK !
            //The max is 15-hl+hl-1 = 14 OK !
            //y-min = 0+0 = 0 Ok!
            //y-max = 15-vL+vL-1 = 14 Ok!
          if c='#' then c:='.';
          if c <> goal_lines[im][jm] then
            result:=FALSE
          end;
        end;
      if result=TRUE then
        begin
        DoLevelCompleted; //Fire event
        exit;
        end;
    end;
  end;

end;

//------------------------------------------------------------------------------
procedure TPlayField.SetMolek_Name(const Value: string);
begin
  FMolek_Name := Value;
end;

//------------------------------------------------------------------------------
//Clear the graphic scene and the textual representative grid
//Frees the ini level file

procedure TPlayField.ClearField;
var
i,j,c:integer;

begin
  if (Selected<>nil) then
    if (Selected.IsMoving) then
        begin
        Cd.Enabled:=FALSE;
        Selected.IsMoving:=FALSE;
        RemoveMover;
        end;
  //Selected is nil (NOT TO FREE. IT's a TRUE pointer)
  //We have to nullify it BEFORE the full clearing
  //below because Selected can have a mover that
  //requires an existing atom on wich operate
  //...see (a-note) in setSelected method;
  Selected:=nil;

  //We have to use inverse for loop because we
  //are freeing list objects; so evry time
  //an object is cleared List count decreases
  //by one and this behavior could cause
  //AV in step-forward order.

(**********************************************)
  FFirstAtom := nil;
(**********************************************)

  c:=Objects.Count-1;

  for i := c downto 0 do
    begin
    if  (Objects.Children[i] is TGLDummyCube)  or
        (Objects.Children[i] is TGLLightSource)   then
        continue;

    Objects.Children[i].Free;
    end;

(**********************************************)
  //Free the goal viewer
  if Assigned(GoalViewer) then
    GoalViewer.ClearField;
  {c:=GoalViewer.Objects.Count-1;

  for i := c downto 0 do
    begin
    if  (GoalViewer.Objects.Children[i] is TGLDummyCube)  or
        (GoalViewer.Objects.Children[i] is TGLLightSource)   then
        continue;

    GoalViewer.Objects.Children[i].Free;
    end;}
(**********************************************)

  //Clean the text grid
  for  i:= 0 to 14 do
    for j := 0 to 14 do
    begin
      PlayGrid[i,j]:='.';
    end;

  //Free Ini level file
  FreeAndNil(LevelFile);
  moves:=0;
end;

//------------------------------------------------------------------------------
//Set the selected atom on the graphical scene applying
//movement arrows on it

procedure TPlayField.SetSelected(const Value: TWatom);
begin
  //(a-note)
  if ( (FSelected<>nil) and (FSelected.IsMoving) ) then exit;
  if FSelected<>nil then RemoveMover;
     FSelected := Value;

  if FSelected<>nil then ApplyMover;
end;

//------------------------------------------------------------------------------
//Applies the movement arrows to the selected atom

procedure TPlayField.ApplyMover;
var
  mv:TGLSprite;
  mv_bmp:TBitmap;
begin
  //mover bitmap init
  mv_bmp:=TBitmap.Create;
  mv_bmp.Height:=32;
  mv_bmp.Width:=32;
  mv_bmp.Transparent:=FALSE;

  if PlayGrid[Selected.Col,Selected.Row-1]='.' then
    begin
    mv:=TGLSprite(Selected.AddNewChild(TGLSprite));
    mv.Height:=CELL_LENGTH;
    mv.Width:=CELL_LENGTH;
    mv.Position.X:= 0;
    mv.Position.Y:= CELL_LENGTH;
    mv.Position.Z:=0;
    mv_bmp.LoadFromResourceName(hInstance,'F_S');
    mv.Material.Texture.Assign(mv_bmp);
    mv.Material.Texture.Disabled:=FALSE;
    mv.Tag:=1;
    end;

  if PlayGrid[Selected.Col-1,Selected.Row]='.' then
    begin
    mv:=TGLSprite(Selected.AddNewChild(TGLSprite));

    mv.Height:=CELL_LENGTH;
    mv.Width:=CELL_LENGTH;

    mv.Position.X:= -CELL_LENGTH;
    mv.Position.Y:= 0;

    mv.Position.Z:=0;
    mv_bmp.LoadFromResourceName(hInstance,'F_SX');
    mv.Material.Texture.Assign(mv_bmp);
    mv.Material.Texture.Disabled:=FALSE;
    mv.Tag:=2;
    end;

  if PlayGrid[Selected.Col+1,Selected.Row]='.' then
    begin
    mv:=TGLSprite(Selected.AddNewChild(TGLSprite));

    mv.Height:=CELL_LENGTH;
    mv.Width:=CELL_LENGTH;

    mv.Position.X:=CELL_LENGTH;
    mv.Position.Y:= 0 ;

    mv.Position.Z:=0;
    mv_bmp.LoadFromResourceName(hInstance,'F_DX');
    mv.Material.Texture.Assign(mv_bmp);
    mv.Material.Texture.Disabled:=FALSE;
    mv.Tag:=3;
    end;

  if PlayGrid[Selected.Col,Selected.Row+1]='.' then
    begin
    mv:=TGLSprite(Selected.AddNewChild(TGLSprite));

    mv.Height:=CELL_LENGTH;
    mv.Width:=CELL_LENGTH;

    mv.Position.X:=0;
    mv.Position.Y:= -CELL_LENGTH;
    mv.Position.Z:=0;
    mv_bmp.LoadFromResourceName(hInstance,'F_G');
    mv.Material.Texture.Assign(mv_bmp);
    mv.Material.Texture.Disabled:=FALSE;
    mv.Tag:=4;
    end;

  mv_bmp.Free;
end;

//------------------------------------------------------------------------------
//Remove arrows from selected atom
procedure TPlayField.RemoveMover;
var
  i:integer;
begin
  if Selected = nil then exit;

  //Arrows are the only atom's children
  for i:=Selected.Count-1 downto 0 do
    Selected.Children[i].Free;
end;

//------------------------------------------------------------------------------
procedure TPlayField.SetMoves(const Value: integer);
begin
  FMoves := Value;
end;

//------------------------------------------------------------------------------
procedure TPlayField.SetOnLevelCompleted(const Value: TNotifyEvent);
begin
  FOnLevelCompleted := Value;
end;

//------------------------------------------------------------------------------
procedure TPlayField.DoLevelCompleted;
begin

  //delegated
  if Assigned(FOnLevelCompleted) then
    FOnLevelCompleted(Self);
end;

//------------------------------------------------------------------------------
//Save the current play in order to continue level later
//It creates a new ini file similar to standard level ones
//copying in it the current PlayGrid

procedure TPlayField.SaveLevel(AFileName:string);
var
  TempIni:TMeminiFile;
  Lst,TempLst:TStringList;
  Line:string;
  i,j:integer;

begin
  TempIni:=TMemIniFile.Create(AFileName);

  Lst:=TStringList.Create;
  Levelfile.GetStrings(Lst);
  //Lst contains levelfile strings

  TempLst:=TStringList.Create;

  //manually adding sections to TempLst
  TempLst.Add('[LEVEL]');
  Line:= LevelFile.ReadString('Level','Name','NaS');

  TempLst.Add('Name='+Line);

  //copying atom_* entries

  for i := 0 to Lst.Count-1 do
    begin
    if pos('atom',Lst[i])<>0 then
      TempLst.Add(Lst[i]);
    end;

  //storing in feld_* entries
  //the current grid

  for i := 0 to 14 do
    begin
    Line:='';

    for j:=0 to 14 do
      Line:=Line+PlayGrid[j,i];

    if i<10 then
      TempLst.Add('feld_'+ IntToHex((i),2)+'='+Line)
    else
      TempLst.Add('feld_'+ IntToStr(i)+'='+Line);
    end;

  Line:='';

  //copying mole_* entries
  for i := 0 to Lst.Count-1 do
    begin
    if pos('mole',Lst[i])<>0 then
      TempLst.Add(Lst[i]);
    end;


  //Adding a [saved] section
  //in order to recreate
  //play environment:
  //only user saved levels
  //have this section

  TempLst.Add('[SAVED]');
  TempLst.Add('moves'+'='+IntToStr(Moves));
(**********************************************)
  //This cause errors when saving a previously loaded game.
  //TempLst.Add('levelnumber'+'='+Copy(ExtractFileName(FLevel),7,3));
  TempLst.Add('levelnumber'+'=' + IntToStr(FLevelNumber));
(**********************************************)

  //Store entries on disk file
  TempIni.SetStrings(TempLst);

  //Write file
  TempIni.UpdateFile;

  TempLst.Free;
  Lst.Free;
  TempIni.Free;
end;

//---------------------------------------------------------------------------
//Load a previously saved level
procedure TPlayField.LoadLevel(AFileName:string);
begin
  Level:=AFileName;
  Moves:=LevelFile.ReadInteger('SAVED','moves',0);
end;


(**********************************************)
procedure TPlayField.AddAtom(ARow, ACol: Integer; AAtomID: Char; ASpec: String);
var
  NewAtom: TWAtom;
begin
  if PlayGrid[ACol, ARow] <> '.' then
    Exit;
  NewAtom := TWatom(Objects.AddNewChild(TWAtom)); //Create atom
  with NewAtom do
  begin
    AtomID:= AAtomID;             //Set the Atom ID
    if AtomID <> '#' then
      Spec := ASpec
    else
    begin
      Material.Texture:=TGLTexture(FWallBitmap);
      Material.Texture.Disabled:=FALSE;
    end;
    Row := ARow;
    Col := ACol;
    Position.X:=-CELL_LENGTH*7 {- CELL_LENGTH/2}+CELL_LENGTH*(Col);
    Position.Y:=CELL_LENGTH*7 {+ CELL_LENGTH/2}-CELL_LENGTH*(Row);
    Position.Z:=0;
    Visible:=TRUE;
    PlayGrid[Col,Row] := AtomID[1];      //Refresh text grid   [0..x]
  end;
  AddAtomToList(NewAtom);
end;
(**********************************************)


(**********************************************)
procedure TPlayField.DeleteAtom(Atom: TWAtom);
begin
  if Atom.IsMoving then
  begin
    Cd.Enabled:=FALSE;
    Atom.IsMoving:=FALSE;
    RemoveMover;
  end;
  if Atom = Selected then
    Selected := nil;
  DeleteAtomFromList(Atom);
  PlayGrid[Atom.Col, Atom.Row] := '.';
  Atom.Free;
end;
(**********************************************)


(**********************************************)
procedure TPlayField.AddWall(Row, Col: Integer);
begin
  DoAddWall(Row, Col, AddWallsAsAtom);
end;
(**********************************************)


(**********************************************)
procedure TPlayField.AddAtomToList(Atom: TWAtom);
var
  AtomPointer: TWAtom;
begin
  if FirstAtom = nil then
  begin
    Atom.NextAtom := Atom;
    FFirstAtom := Atom;
    Exit;
  end;
  AtomPointer := FFirstAtom;
  //travese to end of list.
  while (AtomPointer.NextAtom <> FFirstAtom) do
    AtomPointer := AtomPointer.NextAtom;
  Atom.NextAtom := AtomPointer.NextAtom;
  AtomPointer.NextAtom := Atom;
end;
(**********************************************)

(**********************************************)
procedure TPlayField.DeleteAtomFromList(Atom: TWAtom);
var
  AtomPointer: TWAtom;
begin
  if FFirstAtom = nil then
    Exit;
  AtomPointer := FFirstAtom;
  //traverse to find atom, else reach end of list.
  with AtomPointer do
    while (NextAtom <> Atom) and (NextAtom <> FFirstAtom) do
      AtomPointer := NextAtom;
  //Atom not found.
  if AtomPointer.NextAtom <> Atom then
    Exit;
  AtomPointer.NextAtom := Atom.NextAtom;
  if Atom = FFirstAtom then
  begin
    if FFirstAtom.NextAtom = FFirstAtom then
      FFirstAtom := nil
    else
      FFirstAtom := FFirstAtom.NextAtom;
  end;
end;
(**********************************************)

(**********************************************)
function TPlayField.GetCells(Row, Col: Integer): Char;
begin
  Result := PlayGrid[Col, Row];
end;
(**********************************************)

(**********************************************)
procedure TPlayField.DoAddWall(Row, Col: Integer; AddAsAtom: Boolean);
var
  wl: TGLBaseSceneObject;
begin
  if PlayGrid[Col, Row] <> '.' then
    Exit;
  if AddAsAtom then
    AddAtom(Row, Col, '#', '')
  else
  with TGLSprite(wl) do
  begin
    wl:= TGLSprite(Objects.AddNewChild(TGLSprite));

    TGLSprite(wl).Material.Texture:=TGLTexture(FWallBitmap);
    TGLSprite(wl).Material.Texture.Disabled:=FALSE;

    Position.X:=-CELL_LENGTH*7 {- CELL_LENGTH/2}+CELL_LENGTH*(Col);
    Position.Y:=CELL_LENGTH*7 {+ CELL_LENGTH/2}-CELL_LENGTH*(Row);
    Position.Z:=0;

    Height:=CELL_LENGTH;
    Width:=CELL_LENGTH;

    Visible:=TRUE;

    PlayGrid[Col,Row]:= '#';     //Refresh Array Grid  (#)

  end;
end;
(**********************************************)

//-----------------------------------------------------------------------------

{ TWAtom }

constructor TWAtom.Create(AOWner: TComponent);
begin
  inherited;
  Height:=CELL_LENGTH;
  Width:=CELL_LENGTH;
  Direction.X:=0;
  Direction.Y:=0;
  Direction.Z:=1;
end;

//------------------------------------------------------------------------------
destructor TWAtom.Destroy;
begin
  DeleteChildren;
  inherited;
end;

//------------------------------------------------------------------------------
procedure TWAtom.SetAtomID(const Value: string);
begin
  FAtomId:=Value;
end;

//------------------------------------------------------------------------------
procedure TWAtom.SetCol(const Value: integer);
begin
  FCol := Value;
end;


//------------------------------------------------------------------------------
procedure TWAtom.SetIsMoving(const Value: boolean);
begin
  FIsMoving := Value;
end;

//------------------------------------------------------------------------------
procedure TWAtom.SetMovType(const Value: TMovement);
begin
  FMovType := Value;
end;

//------------------------------------------------------------------------------
procedure TWAtom.SetNextAtom(const Value: TWAtom);
begin
  FNextAtom := Value;
end;

//------------------------------------------------------------------------------
procedure TWAtom.SetRow(const Value: integer);
begin
  FRow := Value;
end;

//----------------------------------------------------------------------------
//fissa le caratteristiche dell'atomo in funione della stringa identificativa
//(En.)Applies chem and binds to the atom using ID string (see above)

procedure TWAtom.SetSpec(const Value: string);
var
  _Chem:char;
  _Bind:string; //Chem contiene la specie chimica, _Bind il/i legame/i
  i:integer;
  temp_atom,temp:TBitmap;
  Binds_graph:TBitmap;
  _B:char;
  r:TRect;

begin
  temp:=TBitmap.Create;
  Binds_graph:=TBitmap.Create;
  temp_atom:=TBitmap.Create;
  Binds_graph.PixelFormat:=pf8bit;
  temp_atom.PixelFormat:=pf8bit;
  temp.PixelFormat:=pf8bit;

  //Bitmap sizes are power of 2
  temp.Height:=32;
  temp.Width:=32;
  temp_atom.Height:=32;
  temp_atom.Width:=32;
  Binds_graph.Height:=32;
  Binds_graph.width:=32;

  r.Left:=0;
  r.Top:=0;
  r.Bottom:=31;
  r.Right:=31;

  //0,0.......0,31
  //..............
  //..............
  //31,0.....31,31

  FSpec:=Value;
  //Value contains informations about atom type and binds
  //as the following schema:
  //<Chemical>-[<bind1><bind2>...<bindn>]
  _Chem:=Value[1];
  _Bind:=Copy(Value,3,8); //Copia in _bind il tipo di legame

  temp_atom.Transparent:=FALSE;

  temp.LoadFromResourceName(hinstance,'BLACK');
  temp.Transparent:=TRUE;


  //Black isthe transparent color
  Binds_graph.LoadFromResourceName(hinstance,'BLACK');
  Binds_graph.Transparent:=TRUE;
  Binds_graph.TransparentMode:=tmFixed;
  Binds_Graph.TransparentColor:=Binds_Graph.Canvas.Pixels[0,0];
  temp.TransparentColor := Binds_Graph.TransparentColor;
  //Specie CHIMICA

  case _Chem of
    '1':
      temp_atom.LoadFromResourceName(hinstance,'IDROGENO');

    '2':
      temp_atom.LoadFromResourceName(hinstance,'CARBONIO');

    '3':
      temp_atom.LoadFromResourceName(hinstance,'OSSIGENO');

    '4':
      temp_atom.LoadFromResourceName(hinstance,'AZOTO');

    '5':
      temp_atom.LoadFromResourceName(hinstance,'ZOLFO');

    '6':
      temp_atom.LoadFromResourceName(hinstance,'FLUORO');

    '7':
      temp_atom.LoadFromResourceName(hinstance,'CLORO');

    '9':
      temp_atom.LoadFromResourceName(hinstance,'FOSFORO');

    'o':
      temp_atom.LoadFromResourceName(hinstance,'CRISTAL');

    'A':
      temp_atom.LoadFromResourceName(hinstance,'L_H');

    'C':
      temp_atom.LoadFromResourceName(hinstance,'L_V');

    'D':
      temp_atom.LoadFromResourceName(hinstance,'L_SWNE');
  end;

  i:=1;
  while i<=length(_Bind) do
    begin
    _B:=_Bind[i];

    case _B of
    'c':
      begin
      temp.LoadFromResourceName(hinstance,'S_E');
      //temp.TransparentColor:=temp.Canvas.Pixels[0,0];
      end;

    'g':
      begin
      temp.LoadFromResourceName(hinstance,'S_W');
      //temp.TransparentColor:=temp.Canvas.Pixels[0,0];
      end;

     'a':
      begin
      temp.LoadFromResourceName(hinstance,'S_N');
      //temp.TransparentColor:=temp.Canvas.Pixels[0,0];
      end;

      'e':
      begin
      temp.LoadFromResourceName(hinstance,'S_S');
      //temp.TransparentColor:=temp.Canvas.Pixels[0,31];
      end;

      'd':
      begin
      temp.LoadFromResourceName(hinstance,'S_SE');
      //temp.TransparentColor:=temp.Canvas.Pixels[0,0];
      end;

      'b':
      begin
      temp.LoadFromResourceName(hinstance,'S_NE');
      //temp.TransparentColor:=temp.Canvas.Pixels[0,0];
      end;

      'h':
      begin
      temp.LoadFromResourceName(hinstance,'S_NW');
      //temp.TransparentColor:=temp.Canvas.Pixels[31,31];
      end;

      'f':
      begin
      temp.LoadFromResourceName(hinstance,'S_SW');
      //temp.TransparentColor:=temp.Canvas.Pixels[0,0];
      end;

      'A':
      begin
      temp.LoadFromResourceName(hinstance,'D_N');
      //temp.TransparentColor:=temp.Canvas.Pixels[0,0];
      end;

      'C':
      begin
      temp.LoadFromResourceName(hinstance,'D_S');
      //temp.TransparentColor:=temp.Canvas.Pixels[0,0];
      end;

      'D':
      begin
      temp.LoadFromResourceName(hinstance,'D_W');
      //temp.TransparentColor:=temp.Canvas.Pixels[0,0];
      end;

      'B':
      begin
      temp.LoadFromResourceName(hinstance,'D_E');
      //temp.TransparentColor:=temp.Canvas.Pixels[0,0];
      end;

      'F':
      begin
      temp.LoadFromResourceName(hinstance,'T_E');
      //temp.TransparentColor:=temp.Canvas.Pixels[0,0];
      end;

      'H':
      begin
      temp.LoadFromResourceName(hinstance,'T_W');
      //temp.TransparentColor:=temp.Canvas.Pixels[0,0];
      end;

      'G':
      begin
      temp.LoadFromResourceName(hinstance,'T_S');
      //temp.TransparentColor:=temp.Canvas.Pixels[0,0];
      end;

      'E':
      begin
      temp.LoadFromResourceName(hinstance,'T_N');
      //temp.TransparentColor:=temp.Canvas.Pixels[0,0];
      end;

    end;

    temp.Transparent:=TRUE;

    //temp_atom.Canvas.CopyMode:=cmSrcCopy;
    //Binds_graph.Canvas.Draw(0,0,temp);
    Binds_graph.Canvas.StretchDraw(r,temp);
    i:=i+1;
  end;

  //temp_atom.Canvas.CopyMode:=cmSrcCopy;
  //temp_atom.Canvas.Draw(0,0,Binds_graph);
  temp_atom.Canvas.StretchDraw(r,Binds_graph);

  material.Texture.TextureMode:=tmDecal;
  material.Texture.Disabled:=TRUE;
  //material.Texture.Image.Assign(temp_atom);
  material.Texture:=TGLTexture(temp_atom);


  //----------------------------------------------------------------------------

  material.Texture.TextureWrap:=twNone;

  //IF THIS PRODUCES A "NOT VALID ENUMERATOR" ERROR
  //LOOK AT LINE 3996 GLTexture.pas and try to change the following
  //" if GL_VERSION_1_2 or GL_EXT_texture_edge_clamp then begin //Original "
  //...with...
  //" if not GL_VERSION_1_2 and not GL_EXT_texture_edge_clamp then begin "//Fede
  //However this is a workaround: the problem is possibly located
  //in display drivers ( try to update )
  //----------------------------------------------------------------------------

  material.Texture.TextureFormat:=tfRGB;
  material.Texture.Disabled:=FALSE;

  //material.Texture.MagFilter:= maNearest;
  //material.Texture.MinFilter:= miNearest; //prevents txtr distortion in gv
  //material.Texture.MinFilter:= miLinear;
  //material.Texture.MinFilter:=miLinearMipMapLinear;
  //material.Texture.MinFilter:=miLinearMipMapNearest;
  //material.Texture.MinFilter:=miNearestMipMapLinear; //this also works...
  //material.Texture.MinFilter:=miNearestMipMapNearest;

  Visible:=TRUE;

  FreeAndNil(temp);
  FreeAndNil(Binds_graph);
  FreeAndNil(Temp_atom);

end;
//---------------------------------------------------------------------------

//--------------------------------
// ironfede@users.sourceforge.net
//--------------------------------

end.

