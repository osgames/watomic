unit editormain;
{
------------------------------------------------------------------
WATOMIC - Windows(c) KAtomic clone

Copyright (C) 2004-2005  Federico Blaseotto (ironfede)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
------------------------------------------------------------------
}

interface

uses
  Windows,  SysUtils, Classes,  Controls, Forms,
  GLScene,  Field,  StdCtrls, Dialogs, Menus, IniFiles, ImgList,
  GLWin32Viewer, GLObjects, ComCtrls, Graphics,
  ExtCtrls, ToolWin, ActnList, Messages;

type
  TMainForm = class(TForm)
    GoalSceneViewer: TGLSceneViewer;
    PFViewer: TGLSceneViewer;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ImageList1: TImageList;
    MainMenu1: TMainMenu;
    About1: TMenuItem;
    OpenDialog1: TOpenDialog;
    SaveDialog1: TSaveDialog;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    File1: TMenuItem;
    Save1: TMenuItem;
    Load1: TMenuItem;
    Close1: TMenuItem;
    N1: TMenuItem;
    Info1: TMenuItem;
    ToolButton8: TToolButton;
    Timer1: TTimer;
    Panel2: TPanel;
    MoleculeNamEdit: TEdit;
    MoleculeNameLabel: TLabel;
    New1: TMenuItem;
    Atom1: TMenuItem;
    AddAtom1: TMenuItem;
    AddWall1: TMenuItem;
    Delete1: TMenuItem;
    MainActionList: TActionList;
    AddAtomAction: TAction;
    DeleteAtomAction: TAction;
    AddWallAction: TAction;
    DuplicateAction: TAction;
    Duplicate1: TMenuItem;
    procedure PlayField1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Close1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure About1Click(Sender: TObject);
    procedure Save1Click(Sender: TObject);
    procedure LoadLevelClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDestroy(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure New1Click(Sender: TObject);
    procedure AddAtomActionExecute(Sender: TObject);
    procedure AddWallActionExecute(Sender: TObject);
    procedure DeleteAtomActionUpdate(Sender: TObject);
    procedure DeleteAtomActionExecute(Sender: TObject);
    procedure DuplicateActionExecute(Sender: TObject);

  private
    { Private declarations }
    CM1,CM2:TGLCamera;
    LS1,LS2:TGLLightSource;
    DC1,DC2:TGLDummyCube;
    setf:TIniFile;
    procedure WMActivate(var Message: TWMActivate); message WM_ACTIVATE;

    procedure GetFinalMolecule(var Grid: TPlayGrid; var RowCount, ColCount: Integer);
    function GetFreeCell(Field: TPlayField; var Row, Col:Integer): Boolean;
    function GetFreeAtomID(var AtomID: Char): Boolean;

    function ShowNewAtomDialog(var AtomSpec: String): Boolean;
  public
    { Public declarations }
    PlayField1:TPlayField;
    GoalScene:TPlayField;
    MainIniFileName:string;

    procedure SaveLevelToFile(AFileName: String);
    procedure LoadLevelFromFile(AFileName: String);
    
    procedure ReadAndApplySettings;
    procedure ChangeWSize(n,w,h:integer);
    procedure ClearField;
    procedure ChangeSelected;
    function InitializeWAtomicScene:boolean;
    function InitializeMainForm:boolean;
  end;

var
  MainForm: TMainForm;

implementation

{$R *.dfm}

uses about,mmsystem,sh_fun, AtomSelect;

//---------------------------------------------------------------------------
//Produces the atom's movements as response to mouse button pressed

procedure TMainForm.PlayField1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  pick:TGLBaseSceneObject;
  Field: TPlayField;
  Viewer: TGLSceneViewer;
begin
  ActiveControl := nil;
   
  if PlayField1.Selected <> nil then
    if PlayField1.Selected.IsMoving then
      Exit;
  if GoalScene.Selected <> nil then
    if GoalScene.Selected.IsMoving then
      Exit;

  Viewer := Sender as TGLSceneViewer;
  if Viewer = PFViewer then
    Field := PlayField1
  else
    Field := GoalScene;

  pick := Viewer.Buffer.GetPickedObject(x, y);
  if pick is TWAtom then
  begin
    PlayField1.Selected := nil;
    GoalScene.Selected := nil;
    Field.Selected:=TWAtom(pick);
  end;

  if (pick<>nil) and (pick.Parent is TWAtom) then
    begin
    case pick.Tag of
    1:
      Field.MoveAtomUp;
    2:
      Field.MoveAtomLeft;
    3:
      Field.MoveAtomRight;
    4:
      Field.MoveAtomDown;
    end;
  end;

end;

//---------------------------------------------------------------------------
procedure TMainForm.ClearField;
begin
  PlayField1.ClearField;
end;

//---------------------------------------------------------------------------
//Arrow keys moves atoms, space bar change selected
procedure TMainForm.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin

  case key of
    VK_LEFT:
      begin
        PlayField1.MoveAtomLeft;
        GoalScene.MoveAtomLeft;
      end;
    VK_RIGHT:
      begin
        PlayField1.MoveAtomRight;
        GoalScene.MoveAtomRight;
      end;
    VK_UP:
      begin
        PlayField1.MoveAtomUp;
        GoalScene.MoveAtomUp;
      end;
    VK_DOWN:
      begin
        PlayField1.MoveAtomDown;
        GoalScene.MoveAtomDown
      end;
    VK_SPACE:
      ChangeSelected;
    end;

    //key:=0;//return 0 as key pressed
    //ActiveControl:=nil;
end;

//---------------------------------------------------------------------------
procedure TMainForm.Close1Click(Sender: TObject);
begin
  ClearField;
  Close;
end;

//---------------------------------------------------------------------------
//Call all initiaization routines:
//InitEnv                  :prepares ini files and directory
//InitializeWAtomicScene   :creates OpenGl scene at runtime
//InitializeMainForm       :Link event handlers and set window size
procedure TMainForm.FormCreate(Sender: TObject);
begin
  if not InitEnv then
    MessageDlg('Directory/Ini files init error',mtError,[mbOk],0);
  if not InitializeWAtomicScene then
    MessageDlg('OpenGL scene initialization error',mtError,[mbOk],0);
  if not InitializeMainForm then
    MessageDlg('Form geometry setting error',mtError,[mbOk],0);
end;

//---------------------------------------------------------------------------
//Display About box

procedure TMainForm.About1Click(Sender: TObject);
var
  ab:TAboutForm;
begin
  ab:=TAboutForm.Create(Self);
  ab.ShowModal;
  ab.Free;
end;

//---------------------------------------------------------------------------
//This procedure allows user to navigate among atoms
//searching for the .NextAtom and pointing
//Selected property to it.
procedure TMainForm.ChangeSelected;
begin
  if PlayField1.Selected<>nil then
    PlayField1.Selected:=PlayField1.Selected.NextAtom;
  if GoalScene.Selected <> nil then
    GoalScene.Selected:=GoalScene.Selected.NextAtom;
  if (PlayField1.Selected = nil) and (GoalScene.Selected = nil) then
    if PlayField1.FirstAtom <> nil then
      PlayField1.Selected := PlayField1.FirstAtom;
end;

//---------------------------------------------------------------------------
//Saves the current gameplay
procedure TMainForm.Save1Click(Sender: TObject);
begin
  if SaveDialog1.Execute then
    SaveLevelToFile(SaveDialog1.FileName);
end;

//---------------------------------------------------------------------------
//Load a saved level
procedure TMainForm.LoadLevelClick(Sender: TObject);
begin
  if OpenDialog1.Execute then
    LoadLevelFromFile(OpenDialog1.FileName);
end;

//--------------------------------------------------------------------------
//Free the settings file and save the high score list
procedure TMainForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ClearField;
  setf.Free;//ini file
end;

//--------------------------------------------------------------------------
//Initialize the scene setting light sources geometrical properties,
//cameras and dummy cube. Take a look at 'scene.txt' for info
//on those properties

function TMainForm.InitializeWAtomicScene:boolean;
begin
  try
    PlayField1:=TPlayField.Create(self);
    GoalScene:=TPlayField.Create(self);
    PlayField1.GoalViewer:=GoalScene;
    GoalSceneViewer.Parent:=Self;
    PFViewer.Parent:=Self;

    PlayField1.SingleMovement := True;
    GoalScene.SingleMovement := True;
    PlayField1.NoCompletion := True;
    GoalScene.NoCompletion := True;
    PlayField1.AddWallsAsAtom := True;
    
    LS1:=TGLLightSource(GoalScene.Objects.AddNewChild(TGLLightSource));
    DC1:=TGLDummyCube(GoalScene.Objects.AddNewChild(TGLDummyCube));
    CM1:=TGLCamera(GoalScene.Cameras.AddNewChild(TGLCamera));

    //clrTransparent
    LS1.Ambient.Alpha:=0;
    LS1.Ambient.Red:=0;
    LS1.Ambient.Blue:=0;
    LS1.Ambient.Green:=0;

    //LS position
    LS1.Position.X:=0;
    LS1.Position.Y:=0;
    LS1.Position.Z:=1;

    LS1.LightStyle:=lsParallel;
    LS1.SpotCutOff:=180;
    LS1.SpotDirection.X:=0;
    LS1.SpotDirection.Y:=0;
    LS1.SpotDirection.Z:=-1;

    //DummyCube
    DC1.Position.X:=0;
    DC1.Position.Y:=0;//0.1;
    DC1.Position.Z:=0;

    DC1.Up.X:=0;
    DC1.Up.Y:=0;
    DC1.Up.Z:=1;

    DC1.Direction.X:=0;
    DC1.Direction.Y:=0;
    DC1.Direction.Z:=1;

    CM1.TargetObject:=DC1;
    CM1.FocalLength:=100;
    CM1.DepthOfView:=100;
    CM1.Direction.X:=0;
    CM1.Direction.Y:=0;
    CM1.Direction.Z:=-1;
    CM1.Position.X:=0;
    CM1.Position.Y:=0;
    CM1.Position.Z:=4;
    CM1.CameraStyle:=csOrthogonal;

    GoalSceneViewer.Camera:=CM1;

    LS2:=TGLLightSource(PlayField1.Objects.AddNewChild(TGLLightSource));
    DC2:=TGLDummyCube(PlayField1.Objects.AddNewChild(TGLDummyCube));
    CM2:=TGLCamera(PlayField1.Cameras.AddNewChild(TGLCamera));

  //clrTransparent
    LS2.Ambient.Alpha:=0;
    LS2.Ambient.Red:=0;
    LS2.Ambient.Blue:=0;
    LS2.Ambient.Green:=0;

  //LS position
    LS2.Position.X:=0;
    LS2.Position.Y:=0;
    LS2.Position.Z:=1;

    LS2.LightStyle:=lsParallel;
    LS2.SpotCutOff:=180;
    LS2.SpotDirection.X:=0;
    LS2.SpotDirection.Y:=0;
    LS2.SpotDirection.Z:=-1;

   //DummyCube
    DC2.Position.X:=0;
    DC2.Position.Y:=0;
    DC2.Position.Z:=0;

    DC2.Up.X:=0;
    DC2.Up.Y:=0;
    DC2.Up.Z:=1;

    DC2.Direction.X:=0;
    DC2.Direction.Y:=0;
    DC2.Direction.Z:=1;

    CM2.TargetObject:=DC2;
    CM2.FocalLength:=100;
    CM2.DepthOfView:=100;
    CM2.Direction.X:=0;
    CM2.Direction.Y:=0;
    CM2.Direction.Z:=-1;
    CM2.Position.X:=0;
    CM2.Position.Y:=0;
    CM2.Position.Z:=4;
    CM2.CameraStyle:=csOrthogonal;
    //M2.SceneScale:=0.01;

    PFViewer.Camera:=CM2;
    RESULT:=TRUE;
  except
    LS1.Free;
    DC1.Free;
    CM1.Free;
    LS2.Free;
    DC2.Free;
    CM2.Free;
    Result:=False;
  end;
end;

//--------------------------------------------------------------------------
procedure TMainForm.FormDestroy(Sender: TObject);
begin
  PlayField1.Free;
  GoalScene.Free;
end;

//--------------------------------------------------------------------------
procedure TMainForm.FormResize(Sender: TObject);
begin

  PFViewer.Height:=Height-100;
  PFViewer.Width:=PFViewer.Height;

  //Here It was possible to use a panel but
  //I dunno if there's any problem with flickering - OGL
  GoalSceneViewer.Top:=PFViewer.Top;
  GoalSceneViewer.Left:= PFViewer.Left + PFViewer.Width + 15;
  GoalSceneViewer.Width:= Width - GoalSceneViewer.Left -20 ;
  GoalSceneViewer.Height:=GoalSceneViewer.Width;
  Panel2.Left:= PFViewer.Left + PFViewer.Width + 15;
  Panel2.Width:= Width - Panel2.Left -20 ;
  Panel2.Top:=PFViewer.TOP + PFViewer.Height - Panel2.Height;

end;

//--------------------------------------------------------------------------
//Link events handler and set geometry of the window
function TMainForm.InitializeMainForm:boolean;
begin

  try
    // all the window geometry is based on PFViewer position
    PFViewer.Top:=40;
    PFViewer.Left:=10;
    Resize;
    setf:=TIniFile.Create(MAIN_INI_FILENAME);

    ReadAndApplySettings;

    Refresh;
    Application.ProcessMessages;
    Result:=TRUE;
  except
    Result:=FALSE;
  end;

end;

//----------------------------------------------------------------------------
//DEBUG (FPS COUNTER)
procedure TMainForm.Timer1Timer(Sender: TObject);
begin
   Caption:=Format('%.1f FPS ',
                   [PFViewer.FramesPerSecond]);
   pfViewer.ResetPerformanceMonitor;
end;

//----------------------------------------------------------------------------
//Modify window size according to screen reolution
procedure TMainForm.ChangeWSize(n,w,h: integer);
begin
case n of

  0:
    begin
      Width:=650;
      Height:=500;
      MainForm.FormResize(nil);
    end;

  1:
    begin
      Width:=780;
      Height:=580;
      MainForm.FormResize(nil);
    end;

  2:
    begin
      Width:=w;
      Height:=h;
    end;
  end;
end;

//------------------------------------------------------------------------------
//Read ini file and update application with new settings
procedure TMainForm.ReadAndApplySettings;
var
  i:integer;
  b:boolean;
begin

  //Speed
  i:=setf.ReadInteger('SETTINGS','speed',5);
  PlayField1.Speed := i;

  (*//Size
  i:=setf.ReadInteger('SETTINGS','wsize',1);
  ChangeWSize(
              i,
              setf.ReadInteger('SETTINGS','width',720),
              setf.ReadInteger('SETTINGS','height',524)
              );*)

  //Frame skipping prevention
  b:=setF.ReadBool('SETTINGS','preventfrskip',FALSE);
  if b then
    PlayField1.Cd.MaxDeltaTime:=0.015
  else
    PlayField1.Cd.MaxDeltaTime:=0;

end;

//------------------------------------

procedure TMainForm.WMActivate(var Message: TWMActivate);
begin
  inherited;
  ActiveControl := nil;
end;

procedure TMainForm.LoadLevelFromFile(AFileName: String);
begin
  PlayField1.Level := AFileName;
  MoleculeNamEdit.Text := PlayField1.Molek_Name;
end;

procedure TMainForm.GetFinalMolecule(var Grid: TPlayGrid; var RowCount,
  ColCount: Integer);
var
  I, J:Integer;
  StartRow, EndRow, StartCol, EndCol: Integer;
  IsEmptyGrid: Boolean;
  IsEmptyLine: Boolean;
begin
  for I := 0 to 14 do
    for J := 0 to 14 do
      Grid[I, J] := GoalScene.Cells[I, J];

  //exit if grid is empty   
  IsEmptyGrid := True;
  for I := 0 to 14 do
    for J := 0 to 14 do
      if Grid[I, J] <> '.' then
      begin
        IsEmptyGrid := False;
        Break;
      end;
  if IsEmptyGrid then
  begin
    RowCount := 0;
    ColCount := 0;
    Exit;
  end;

  //find first and last no empty row
  StartRow := 14;
  EndRow := 0;
  for I := 0 to 14 do
  begin
    IsEmptyLine := True;
    for J := 0 to 14 do
      if GoalScene.Cells[I, J] <> '.' then
        IsEmptyLine := False;
    if not IsEmptyLine then
    begin
      if I < StartRow then
        StartRow := I;
      if I > EndRow then
        EndRow := I;
    end;
  end;

  //find first and last no empty column
  StartCol := 14;
  EndCol := 0;
  for I := 0 to 14 do
  begin
    IsEmptyLine := True;
    for J := 0 to 14 do
      if GoalScene.Cells[J, I] <> '.' then
        IsEmptyLine := False;
    if not IsEmptyLine then
    begin
      if I < StartCol then
        StartCol := I;
      if I > EndCol then
        EndCol := I;
    end;
  end;

  RowCount := EndRow - StartRow + 1;
  ColCount := EndCol - StartCol + 1;
  
  //copy submatrix to (0, 0)
  for I := 0 to RowCount - 1 do
    for J := 0 to ColCount - 1 do
      Grid[I, J] := Grid[StartRow + I, StartCol + J];
end;

procedure TMainForm.SaveLevelToFile(AFileName: String);
var
  IniFile: TMemIniFile;
  I, J: Integer;
  NumberStr, Line: String;
  FirstAtom, TempAtom: TWAtom;
  Grid: TPlayGrid;
  RowCount, ColCount: Integer;
begin
  IniFile := TMemIniFile.Create('');
  try
    IniFile.WriteString('Level', 'Name', MoleculeNamEdit.Text);

    FirstAtom := PlayField1.FirstAtom;
    TempAtom := FirstAtom;
    if FirstAtom <> nil then
    repeat
      if TempAtom.AtomID <> '#' then
        IniFile.WriteString('Level', 'atom_' + TempAtom.AtomID, TempAtom.Spec);
      TempAtom := TempAtom.NextAtom;
    until TempAtom = FirstAtom;

    for I := 0 to 14 do
    begin
      Line := '';
      for J := 0 to 14 do
        Line := Line + PlayField1.Cells[I, J];
      NumberStr := 'feld_';
      if I < 10 then
        NumberStr := NumberStr + '0';
      NumberStr := NumberStr + IntToStr(I);
      IniFile.WriteString('Level', NumberStr, Line);
    end;

    GetFinalMolecule(Grid, RowCount, ColCount);
    for I := 0 to RowCount - 1 do
    begin
      Line := '';
      for J := 0 to ColCount - 1 do
        Line := Line + Grid[I, J];
      IniFile.WriteString('Level', 'mole_' + IntToStr(I), Line);
    end;

    IniFile.Rename(AFileName, False);
    IniFile.UpdateFile;
  finally
    IniFile.Free;
  end;
end;

procedure TMainForm.New1Click(Sender: TObject);
begin
  ClearField;
end;

function TMainForm.GetFreeCell(Field: TPlayField; var Row, Col: Integer): Boolean;
var
  I, J: Integer;
begin
  Result := False;
  for I := 0 to 14 do
    for J := 0 to 14 do
      if Field.Cells[I, J] = '.' then
      begin
        Row := I;
        Col := J;
        Result := True;
        Exit;
      end;
end;

function TMainForm.GetFreeAtomID(var AtomID: Char): Boolean;
var
  FirstAtom, TempAtom: TWAtom;
  AtomIDExist: array [Char] of Boolean;
  I: Char;
const
  AtomIDSet = ['1'..'9', 'a'..'z'];
begin
  for I := Low(AtomIDExist) to High(AtomIDExist) do
    AtomIDExist[I] := False;
  FirstAtom := PlayField1.FirstAtom;
  TempAtom := FirstAtom;
  if FirstAtom <> nil then
  repeat
    if TempAtom.AtomID <> '#' then
      AtomIDExist[TempAtom.AtomID[1]] := True;
    TempAtom := TempAtom.NextAtom;
  until TempAtom = FirstAtom;
  Result := False;
  for I := Low(AtomIDExist) to High(AtomIDExist) do
    if not AtomIDExist[I] and (I in AtomiDSet) then
    begin
      AtomID := I;
      Result := True;
      Exit;
    end;
end;

procedure TMainForm.AddAtomActionExecute(Sender: TObject);
var
  Row1, Col1, Row2, Col2: Integer;
  FreeAtomID: Char;
  NewSpec: String;
  FirstAtom, TempAtom: TWAtom;
begin
  //get a free atom cell
  if not GetFreeCell(PlayField1, Row1, Col1) or
     not GetFreeCell(GoalScene, Row2, Col2) then
  begin
    MessageDlg('No free cell exist.', mtInformation, [mbOK], 0);
    Exit;
  end;

  //get a free Atom ID
  if not GetFreeAtomID(FreeAtomID) then
  begin
    MessageDlg('Too many atom types exist.', mtInformation, [mbOK], 0);
    Exit;
  end;

  NewSpec := '';
  if not ShowNewAtomDialog(NewSpec) then
    Exit;

  FirstAtom := PlayField1.FirstAtom;
  TempAtom := FirstAtom;
  if FirstAtom <> nil then
  repeat
    if TempAtom.Spec = NewSpec then
    begin
      FreeAtomID := TempAtom.AtomID[1];
      Break;
    end;
    TempAtom := TempAtom.NextAtom;
  until TempAtom = FirstAtom;

  PlayField1.AddAtom(Row1, Col1, FreeAtomID, NewSpec);
  GoalScene.AddAtom(Row2, Col2, FreeAtomID, NewSpec);
  PlayField1.Selected := nil;
  GoalScene.Selected := nil;

  ActiveControl := nil;
end;

procedure TMainForm.AddWallActionExecute(Sender: TObject);
var
  Row1, Col1: Integer;
begin
  if not GetFreeCell(PlayField1, Row1, Col1) then
  begin
    MessageDlg('No free cell exist.', mtInformation, [mbOK], 0);
    Exit;
  end;
  PlayField1.AddWall(Row1, Col1);
  PlayField1.Selected := nil;
  ActiveControl := nil;
end;

function TMainForm.ShowNewAtomDialog(var AtomSpec: String): Boolean;
var
  AtomSelectForm: TAtomSelectForm;
begin
  Result := False;
  AtomSelectForm:= TAtomSelectForm.Create(Self);
  try
    AtomSelectForm.Spec := AtomSpec;
    if AtomSelectForm.ShowModal = mrOk then
    begin
      AtomSpec := AtomSelectForm.Spec;
      Result := True;
    end;
  finally
    AtomSelectForm.Free;
  end;
end;

procedure TMainForm.DeleteAtomActionUpdate(Sender: TObject);
var
  ActionEnabled: Boolean;
begin
  ActionEnabled := False;
  
  if Assigned(PlayField1.Selected)then
    if not PlayField1.Selected.IsMoving then
      ActionEnabled := True;

  if Assigned(GoalScene.Selected)then
    if not GoalScene.Selected.IsMoving then
      ActionEnabled := True;

  (Sender as TAction).Enabled := ActionEnabled;
end;

procedure TMainForm.DeleteAtomActionExecute(Sender: TObject);
var
  Field, OtherField: TPlayField;
  FirstAtom, TempAtom: TWAtom;
begin
  if PlayField1.Selected <> nil then
  begin
    Field := PlayField1;
    OtherField := GoalScene;
  end
  else
  begin
    Field := GoalScene;
    OtherField := PlayField1;
  end;

  if Field.Selected.AtomID <> '#' then
  begin
    FirstAtom := OtherField.FirstAtom;
    TempAtom := FirstAtom;
    if FirstAtom <> nil then
    repeat
      if TempAtom.Spec = Field.Selected.Spec then
      begin
        OtherField.DeleteAtom(TempAtom);
        Break;
      end;
      TempAtom := TempAtom.NextAtom;
    until TempAtom = FirstAtom;
  end;

  Field.DeleteAtom(Field.Selected);
end;

procedure TMainForm.DuplicateActionExecute(Sender: TObject);
var
  Row1, Col1, Row2, Col2: Integer;
  Atom: TWAtom;
begin
  if PlayField1.Selected <> nil then
    Atom := PlayField1.Selected
  else
    Atom := GoalScene.Selected;
    
  if not GetFreeCell(PlayField1, Row1, Col1) or
    ((Atom.AtomID <> '#') and not GetFreeCell(GoalScene, Row2, Col2)) then
  begin
    MessageDlg('No free cell exist.', mtInformation, [mbOK], 0);
    Exit;
  end;

  PlayField1.AddAtom(Row1, Col1, Atom.AtomID[1], Atom.Spec);
  PlayField1.Selected := nil;
  if Atom.AtomID <> '#' then
  begin
    GoalScene.AddAtom(Row2, Col2, Atom.AtomID[1], Atom.Spec);
    GoalScene.Selected := nil;
  end;  
end;

end.
//--------------------------------
// ironfede@users.sourceforge.net
//--------------------------------

