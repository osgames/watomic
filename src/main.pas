unit main;
{
------------------------------------------------------------------
WATOMIC - Windows(c) KAtomic clone

Copyright (C) 2004-2005  Federico Blaseotto (ironfede)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
------------------------------------------------------------------
}

interface

uses
  Windows,  SysUtils, Classes,  Controls, Forms,
  GLScene,   StdCtrls, Dialogs, Menus, IniFiles, ImgList,
  GLWin32Viewer, GLMisc, GLObjects, ComCtrls, Graphics,
  ExtCtrls, ToolWin, Shellapi, Field, HTTPSend;

type
  TMainForm = class(TForm)
    GoalSceneViewer: TGLSceneViewer;
    PFViewer: TGLSceneViewer;
    Label6: TLabel;
    lblMvN: TLabel;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ImageList1: TImageList;
    Panel1: TPanel;
    Panel2: TPanel;
    MainMenu1: TMainMenu;
    Edit1: TMenuItem;
    Help: TMenuItem;
    Settings1: TMenuItem;
    OpenDialog1: TOpenDialog;
    SaveDialog1: TSaveDialog;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    File1: TMenuItem;
    Save1: TMenuItem;
    Load1: TMenuItem;
    Close1: TMenuItem;
    N1: TMenuItem;
    Info1: TMenuItem;
    HighScores1: TMenuItem;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    GetInfoBtn: TButton;
    LevelSB: TScrollBar;
    StaticText1: TStaticText;
    StaticText2: TStaticText;
    Label1: TLabel;
    lblRecord: TLabel;
    Timer1: TTimer;
    ToolButton9: TToolButton;
    Reset1: TMenuItem;
    Help1: TMenuItem;
    View1: TMenuItem;
    procedure PlayField1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormActivate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Close1Click(Sender: TObject);
    procedure Settings1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure About1Click(Sender: TObject);
    procedure LevelChange(Sender: TObject);
    procedure Save1Click(Sender: TObject);
    procedure LoadLevelClick(Sender: TObject);
    procedure HighScores1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDestroy(Sender: TObject);
    procedure GetInfoBtnClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure Reset1Click(Sender: TObject);
    procedure Help1Click(Sender: TObject);

  private
    { Private declarations }
    CM1,CM2:TGLCamera;
    LS1,LS2:TGLLightSource;
    DC1,DC2:TGLDummyCube;
    setf:TIniFile;
    FMoves: integer;


    //OnChangeEnabled indicates if a
    //level loading is(TRUE) or is not(FALSE)
    //the consequence of scrollbar position change by user

    OnChangeEnabled:boolean;
    procedure SetMoves(const Value: integer);

  public
    { Public declarations }
    PlayField1:TPlayField;
    GoalScene:TGLScene;
    MainIniFileName:string;
    DefaultBrowser:string;

    property Moves:integer read FMoves write SetMoves;

    procedure ReadAndApplySettings;
    procedure ChangeWSize(n,w,h:integer);
    procedure ClearField;
    procedure ChangeSelected;
    function LoadSavedLevel(ALevelName:string):boolean;
    function LoadLevelByNumber(ALevelNumber:integer):boolean;
    procedure LevelCompleted(Sender:TObject);
    function InitializeWAtomicScene:boolean;
    function InitializeMainForm:boolean;
  end;

var
  MainForm: TMainForm;

implementation

{$R *.dfm}

uses opt,about,mmsystem,highscr,sh_fun;

//---------------------------------------------------------------------------
//Produces the atom's movements as response to mouse button pressed

procedure TMainForm.PlayField1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  pick:TGLBaseSceneObject;
begin

  pick:=PFViewer.Buffer.GetPickedObject(x, y);
  if (pick is TWAtom)  then
    PlayField1.Selected:=TWAtom(pick);

  if (pick<>nil) and (pick.Parent is TWAtom) then
    begin
    case pick.Tag of
    1:
      PlayField1.MoveAtomUp;
    2:
      Playfield1.MoveAtomLeft;
    3:
      PlayField1.MoveAtomRight;
    4:
      PlayField1.MoveAtomDown;
    end;

    Moves:=PlayField1.Moves;
  end;

  ActiveControl:=nil;
end;

//---------------------------------------------------------------------------
procedure TMainForm.FormActivate(Sender: TObject);
begin
  //PlayField init
  PlayField1.GoalViewer:=GoalScene;
  PlayField1.Level:=ExtractFilePath(Application.ExeName)+'levels\level_1';
  StaticText1.Caption:= PlayField1.Molek_Name;
  GetInfoBtn.Caption:=  'About ' + PlayField1.Molek_Name+'...';

  ActiveControl:=nil;
  //This is needed to avoid the level edit
  //intercept arrows key pressure after mainfrm key preview
  OnChangeEnabled:=TRUE;

  if FileExists(ExtractFilePath(Application.ExeName)+'hs.ini') then
    hsfrm.LoadHS(PlayField1.Molek_Name);

  lblRecord.Caption:=hsfrm.GetRecord(PlayField1.Molek_Name);

end;

//---------------------------------------------------------------------------
procedure TMainForm.ClearField;
begin
  PlayField1.ClearField;
end;

//---------------------------------------------------------------------------
//Arrow keys moves atoms, space bar change selected
procedure TMainForm.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin

  case key of
    VK_LEFT:
      PlayField1.MoveAtomLeft;

    VK_RIGHT:
      PlayField1.MoveAtomRight;

    VK_UP:
      PlayField1.MoveAtomUp;

    VK_DOWN:
      PlayField1.MoveAtomDown;

    VK_SPACE:
      ChangeSelected;
    end;

    key:=0;//return 0 as key pressed
    ActiveControl:=nil;
    Moves:=PlayField1.Moves;//Refresh moves
end;

//---------------------------------------------------------------------------
procedure TMainForm.Close1Click(Sender: TObject);
begin
  ClearField;
  Close;
end;

//---------------------------------------------------------------------------
procedure TMainForm.Settings1Click(Sender: TObject);
var
  optf:TOptForm;

begin
  optf:=TOptForm.Create(self);
  optF.ShowModal;
  ReadAndApplySettings;
  optF.Free;
  ActiveControl:=nil;
end;

//---------------------------------------------------------------------------
//Call all initiaization routines:
//InitEnv                  :prepares ini files and directory
//InitializeWAtomicScene   :creates OpenGl scene at runtime
//InitializeMainForm       :Link event handlers and set window size

procedure TMainForm.FormCreate(Sender: TObject);
begin

  if not InitEnv then
    MessageDlg('Directory/Ini files init error',mtError,[mbOk],0);
  if not InitializeWAtomicScene then
    MessageDlg('OpenGL scene initialization error',mtError,[mbOk],0);
  if not InitializeMainForm then
    MessageDlg('Form geometry setting error',mtError,[mbOk],0);
end;

//---------------------------------------------------------------------------
//Display About box

procedure TMainForm.About1Click(Sender: TObject);
var
  ab:TAboutForm;
begin
  ab:=TAboutForm.Create(Self);
  ab.ShowModal;
  ab.Free;
  ActiveControl:=nil;
end;

//---------------------------------------------------------------------------
//Updates moves number and linked label

procedure TMainForm.SetMoves(const Value: integer);
begin
  FMoves := Value;
  if Value = 0 then
    lblMvn.Caption:=''
  else
    lblMvn.Caption:=IntToStr(Value);
end;

//---------------------------------------------------------------------------
//This procedure allows user to navigate among atoms
//searching for the .NextAtom and pointing
//Selected property to it. If no atom is initially selected
//then procedure scan the scene object's list fo find one.

procedure TMainForm.ChangeSelected;
var
  c,i:integer;
begin
  if PlayField1.Selected<>nil then
    begin
    if PlayField1.Selected.IsMoving then exit;
    PlayField1.Selected:=PlayField1.Selected.NextAtom;
    end
  else
    begin
    c:=PlayField1.Objects.Count-1;
    for i := 0 to c do
      begin
      if  (PlayField1.Objects.Children[i] is TWAtom) then
        begin
        PlayField1.Selected:=TWAtom(PlayField1.Objects.children[i]);
        exit;
        end;//if
      end;//for

    end;//else

end;

//---------------------------------------------------------------------------
//Change level event generated from a level complete (standard load)
//or from a level load from file

procedure TMainForm.LevelChange(Sender: TObject);
begin
  //If loading a custom saved level then exit
  if not OnChangeEnabled then exit;
    LoadLeveLByNumber(LevelSB.Position);

  //Debug
  {memo1.Clear;
  c:=PlayField1.Objects.Count-1;

  for i := c downto 0 do
    memo1.Lines.Add(PlayField1.Objects.Children[i].ClassName);}
end;

//---------------------------------------------------------------------------
//Shows congratulations, plays sounds makes coffee:) and checks for record
procedure TMainForm.LevelCompleted(Sender: TObject);
var
  PlayerName:string;
begin
  PFViewer.Refresh;
  PlayerName:='';
  PlaySound(PAnsiChar(ExtractFilePath(Application.ExeName)+'sounds\level_completed.wav'),0,SND_ASYNC);
  ShowMessage('You have completed '+
    PlayField1.Molek_Name +' in '+
    IntToStr(Moves) +
    ' moves'+#13#10+
    '             Congratulations !');

  //Do we have a new record ?
  if hsfrm.IsRecord(PlayField1.Molek_Name,PlayField1.Moves) then
    begin
    if InputQuery('RECORD !!!!','Please insert your name',PlayerName) then
      begin
      if not (PlayerName = '') then
        hsfrm.AddHighScore(PlayField1.Molek_Name,PlayField1.Moves,PlayerName);

      hsfrm.LoadHS(PlayField1.Molek_Name);
      hsfrm.ShowModal;
      end;
    end
  else
    begin
    if InputQuery('Well done !','Please insert your name',PlayerName) then
      begin
      if not (PlayerName = '') then
        hsfrm.AddHighScore(PlayField1.Molek_Name,PlayField1.Moves,PlayerName);

      hsfrm.LoadHS(PlayField1.Molek_Name);
      hsfrm.ShowModal;
      end;
    end;

  if PlayField1.LevelNumber<>85 then
    LevelSB.Position:=LevelSB.Position+1
  else
  begin
    SHOWMESSAGE('WOW !!! THIS WAS THE LAST LEVEL.... (to be continued)');
    OnChangeEnabled:=FALSE;
    LevelSB.Position := 1;
    LoadLevelByNumber(1);
    OnChangeEnabled:=TRUE;
  end;
end;

//---------------------------------------------------------------------------
//Saves the current gameplay

procedure TMainForm.Save1Click(Sender: TObject);
begin
  if SaveDialog1.Execute then
    PlayField1.SaveLevel(Savedialog1.FileName);

    ActiveControl:=nil;
end;

//---------------------------------------------------------------------------
//Load a saved level

procedure TMainForm.LoadLevelClick(Sender: TObject);
begin
  if OpenDialog1.Execute then
    LoadSavedLevel(OpenDialog1.FileName);

  ActiveControl:=nil;
end;


//--------------------------------------------------------------------------
procedure TMainForm.HighScores1Click(Sender: TObject);
begin
  hsfrm.LoadHS(PlayField1.Molek_Name);
  hsfrm.ShowModal;
  PFViewer.Repaint;
  GoalSceneViewer.Repaint;
  ActiveControl:=nil;
end;

//--------------------------------------------------------------------------
//Free the settings file and save the high score list

procedure TMainForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ClearField;
  setf.Free;//ini file
end;

//--------------------------------------------------------------------------
//Initialize the scene setting light sources geometrical properties,
//cameras and dummy cube. Take a look at 'scene.txt' for info
//on those properties

function TMainForm.InitializeWAtomicScene:boolean;
begin
  try
    PlayField1:=TPlayField.Create(self);
    GoalScene:=TGLScene.Create(self);
    PlayField1.GoalViewer:=GoalScene;
    GoalSceneViewer.Parent:=Self;
    PFViewer.Parent:=Self;

    LS1:=TGLLightSource(GoalScene.Objects.AddNewChild(TGLLightSource));
    DC1:=TGLDummyCube(GoalScene.Objects.AddNewChild(TGLDummyCube));
    CM1:=TGLCamera(GoalScene.Cameras.AddNewChild(TGLCamera));

    //clrTransparent
    LS1.Ambient.Alpha:=0;
    LS1.Ambient.Red:=0;
    LS1.Ambient.Blue:=0;
    LS1.Ambient.Green:=0;

    //LS position
    LS1.Position.X:=0;
    LS1.Position.Y:=0;
    LS1.Position.Z:=1;

    LS1.LightStyle:=lsParallel;
    LS1.SpotCutOff:=180;
    LS1.SpotDirection.X:=0;
    LS1.SpotDirection.Y:=0;
    LS1.SpotDirection.Z:=-1;

    //DummyCube
    DC1.Position.X:=0;
    DC1.Position.Y:=0.1;
    DC1.Position.Z:=0;

    DC1.Up.X:=0;
    DC1.Up.Y:=0;
    DC1.Up.Z:=1;

    DC1.Direction.X:=0;
    DC1.Direction.Y:=0;
    DC1.Direction.Z:=1;

    CM1.TargetObject:=DC1;
    CM1.FocalLength:=100;
    CM1.DepthOfView:=100;
    CM1.Direction.X:=0;
    CM1.Direction.Y:=0;
    CM1.Direction.Z:=-1;
    CM1.Position.X:=0;
    CM1.Position.Y:=0;
    CM1.Position.Z:=4;
    CM1.CameraStyle:=csOrthogonal;

    GoalSceneViewer.Camera:=CM1;

    LS2:=TGLLightSource(PlayField1.Objects.AddNewChild(TGLLightSource));
    DC2:=TGLDummyCube(PlayField1.Objects.AddNewChild(TGLDummyCube));
    CM2:=TGLCamera(PlayField1.Cameras.AddNewChild(TGLCamera));

  //clrTransparent
    LS2.Ambient.Alpha:=0;
    LS2.Ambient.Red:=0;
    LS2.Ambient.Blue:=0;
    LS2.Ambient.Green:=0;

  //LS position
    LS2.Position.X:=0;
    LS2.Position.Y:=0;
    LS2.Position.Z:=1;

    LS2.LightStyle:=lsParallel;
    LS2.SpotCutOff:=180;
    LS2.SpotDirection.X:=0;
    LS2.SpotDirection.Y:=0;
    LS2.SpotDirection.Z:=-1;

   //DummyCube
    DC2.Position.X:=0;
    DC2.Position.Y:=0;
    DC2.Position.Z:=0;

    DC2.Up.X:=0;
    DC2.Up.Y:=0;
    DC2.Up.Z:=1;

    DC2.Direction.X:=0;
    DC2.Direction.Y:=0;
    DC2.Direction.Z:=1;

    CM2.TargetObject:=DC2;
    CM2.FocalLength:=100;
    CM2.DepthOfView:=100;
    CM2.Direction.X:=0;
    CM2.Direction.Y:=0;
    CM2.Direction.Z:=-1;
    CM2.Position.X:=0;
    CM2.Position.Y:=0;
    CM2.Position.Z:=4;
    CM2.CameraStyle:=csOrthogonal;
    //M2.SceneScale:=0.01;

    PFViewer.Camera:=CM2;
    RESULT:=TRUE;
  except
    LS1.Free;
    DC1.Free;
    CM1.Free;
    LS2.Free;
    DC2.Free;
    CM2.Free;
    Result:=False;
  end;
end;

//--------------------------------------------------------------------------
procedure TMainForm.FormDestroy(Sender: TObject);
begin
  PlayField1.Free;
  GoalScene.Free;
end;

//--------------------------------------------------------------------------
procedure TMainForm.GetInfoBtnClick(Sender: TObject);
const
  CGI_ADDR = 'http://watomic.sourceforge.net/cgi-bin/getinfo3.cgi';
var
  HTTP: THTTPSend;
  s:PAnsiChar;
  
begin
  HTTP := THTTPSend.Create;
  s:=nil;

  try
    if HTTP.HTTPMethod('GET', CGI_ADDR+'?level=' +
                     IntToStr(MainForm.PlayField1.LevelNumber)) then
    begin
      GetMem(s,HTTP.Document.size+1);
      FillChar(s^,HTTP.Document.size+1,0);
      HTTP.Document.Read(s^,HTTP.Document.size);
      ShellExecute(0,
                   PAnsiChar('open'),
                   PansiChar(DefaultBrowser),
                   s,
                   nil,
                   SW_SHOW);
    end
    else
      MessageDlg('Communication error: check your internet connection',
                 mtWarning,
                 [mbOk],
                 0);
  finally
    if s<>nil then FreeMem(s);
    HTTP.Free;
  end;
end;

//--------------------------------------------------------------------------
procedure TMainForm.FormResize(Sender: TObject);
begin

  PFViewer.Height:=Height-112;
  PFViewer.Width:=PFViewer.Height;
  PFViewer.Top:=48;

  //Here It was possible to use a panel but
  //I dunno if there's any problem with flickering - OGL
  Panel1.Top:=PFViewer.Top;
  Panel1.Left:= PFViewer.Left + PFViewer.Width + 15;
  Panel1.Width:= Width - Panel1.Left -20 ;
  GoalSceneViewer.Top:=Panel1.Top+Panel1.Height+15;
  GoalSceneViewer.Left:= PFViewer.Left + PFViewer.Width + 15;
  GoalSceneViewer.Width:= Width - GoalSceneViewer.Left -20 ;
  GoalSceneViewer.Height:=GoalSceneViewer.Width;
  GetInfoBtn.Left:= PFViewer.Left + PFViewer.Width + 15;
  Panel2.Left:= PFViewer.Left + PFViewer.Width + 15;
  Panel2.Width:= Width - Panel2.Left -20 ;
  Panel2.Top:=PFViewer.TOP + PFViewer.Height - Panel2.Height;
  GetInfoBtn.Top:=
    ( ( (GoalSceneViewer.Top + GoalSceneViewer.Height) + Panel2.Top )div 2) -
    (GetInfoBtn.Height div 2);

  GetInfoBtn.Width:= Width - GetInfoBtn.Left -20  ;


  StaticText1.Left:=GoalSceneViewer.Left+7;
  StaticText2.Left:=GoalSceneViewer.Left+7;
  StaticText1.Top:=GoalSceneViewer.Top+Goalsceneviewer.Height-35;
  StaticText2.top:=GoalSceneViewer.Top+GoalSceneViewer.Height-60;

end;

//--------------------------------------------------------------------------
procedure TMainForm.FormShow(Sender: TObject);
begin
  ActiveControl:=nil;
end;

//--------------------------------------------------------------------------
//Link events handler and set geometry of the window
//Load setup values from inifile
function TMainForm.InitializeMainForm:boolean;
begin

  try
  // all the window geometry is based on PFViewer position
  PFViewer.Top:=40;
  PFViewer.Left:=10;
  Resize;
  setf:=TIniFile.Create(MAIN_INI_FILENAME);
  PlayField1.OnLevelCompleted:=LevelCompleted;

  ReadAndApplySettings;

  Refresh;
  Application.ProcessMessages;
  Result:=TRUE;
  except
  Result:=FALSE;
  end;

end;

//----------------------------------------------------------------------------
//DEBUG (FPS COUNTER)
procedure TMainForm.Timer1Timer(Sender: TObject);
begin
   //Caption:=Format('%.1f FPS ',
   //                [PFViewer.FramesPerSecond]);
   //pfViewer.ResetPerformanceMonitor;
end;

//----------------------------------------------------------------------------
//Modify window size according to screen resolution
procedure TMainForm.ChangeWSize(n,w,h: integer);
begin
case n of

  0:
    begin
      Width:=650;
      Height:=500;
      MainForm.FormResize(nil);
    end;

  1:
    begin
      Width:=780;
      Height:=580;
      MainForm.FormResize(nil);
    end;

  2:
    begin
      Width:=w;
      Height:=h;
    end;
  end;
end;

//------------------------------------------------------------------------------
//Read ini file and update application with new settings

procedure TMainForm.ReadAndApplySettings;
var
  i:integer;
  b:boolean;
begin

  //Speed
  i := setf.ReadInteger('SETTINGS','speed',5);
  PlayField1.Speed := i;

  //Size
  i:=setf.ReadInteger('SETTINGS','wsize',1);
  ChangeWSize(
              i,
              setf.ReadInteger('SETTINGS','width',720),
              setf.ReadInteger('SETTINGS','height',524)
              );

  //Frame skipping prevention
  b:=setF.ReadBool('SETTINGS','preventfrskip',FALSE);
  if b then
    PlayField1.Cd.MaxDeltaTime:=0.015
  else
    PlayField1.Cd.MaxDeltaTime:=0;

  //Default browser for infopages
  i:=setf.ReadInteger('SETTINGS','browser',0);
  case i of
    0: DefaultBrowser:='iexplore';
    1: DefaultBrowser:='firefox';
  end;

end;

//-----------------------------------------------------------------------------
//Restart the current level

procedure TMainForm.Reset1Click(Sender: TObject);
begin
  LoadLevelByNumber(PlayField1.LevelNumber);
  Moves:= 0;
  ActiveControl:= nil;
end;

//------------------------------------------------------------------------------
//Load a level given a level filename
//Level is a user saved one

function TMainForm.LoadSavedLevel(ALevelName: string): boolean;
begin
  RESULT:=TRUE;

  //Disable OnChange event preventing
  //LevelSB to refire event
  OnChangeEnabled:=FALSE;

  if FileExists(ALevelName) then
    PlayField1.Level:= ALevelName
  else
    begin
    RESULT:=FALSE;
    exit;
    end;

  LevelSB.Position := PlayField1.LevelNumber;
  StaticText2.Caption :=IntToStr(PlayField1.LevelNumber);
  StaticText1.Caption := PlayField1.Molek_Name;
  GetInfoBtn.Caption :=  'About ' + PlayField1.Molek_Name;
  lblRecord.Caption := hsfrm.GetRecord(PlayField1.Molek_Name);

  Moves:=PlayField1.Moves;
  GoalSceneViewer.Repaint;//refresh playfield
  PFViewer.Repaint;

  OnChangeEnabled:=TRUE;

end;

//------------------------------------------------------------------------------
//Load a level given its number (by the scrollbar)
//Level is located in standard levels dir

function TMainForm.LoadLevelByNumber(ALevelNumber: integer): boolean;
var
  f:string;
begin
  Result:=TRUE;

  f:=ExtractFilePath(Application.ExeName)+'levels\level_' + IntToStr(ALevelNumber);
  if FileExists(f) then
    PlayField1.Level:= f
  else
    begin
    Result:=FALSE;
    exit;
    end;

  StaticText2.Caption:= IntToStr(ALevelNumber);
  StaticText1.Caption:= PlayField1.Molek_Name;
  GetInfoBtn.Caption:=  'About ' + PlayField1.Molek_Name + '...';
  lblRecord.Caption:=hsfrm.GetRecord(PlayField1.Molek_Name);
  Moves:=PlayField1.Moves;
  GoalSceneViewer.Buffer.Render;   //Correct use ? To be investigated...
  PFViewer.Buffer.Render;

  ActiveControl:= nil;
end;

//------------------------------------------------------------------------------
//Open the html help file (Thanks to Mehdi Farhadi)
//We don't want use HtmlHelp API at the moment

procedure TMainForm.Help1Click(Sender: TObject);
begin
  ShellExecute(0,'open',PCHar(ExtractFileDir(Application.exename)+'\WAtomic.chm'),nil,nil,SW_SHOWNORMAL);
end;

end.
//--------------------------------
// ironfede@users.sourceforge.net
//--------------------------------
