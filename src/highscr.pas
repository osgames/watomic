unit highscr;
{
------------------------------------------------------------------
WATOMIC - Windows(c) KAtomic clone

Copyright (C) 2004  Federico Blaseotto (ironfede)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
------------------------------------------------------------------
}
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ComCtrls, StdCtrls,IniFiles;

type
  Thsfrm = class(TForm)
    ListView1: TListView;
    Panel1: TPanel;
    OkBtn: TButton;
    procedure FormActivate(Sender: TObject);

  private
    { Private declarations }

  public
    { Public declarations }
    ActiveMolecule:string;

    function IsRecord(AMolecule:string; Ascore:integer):boolean;
    procedure AddHighScore(Amolecule:string; AScore:integer;APlayerName:string);
    procedure LoadHS(AMolecule:string);
    function GetRecord(AMolecule:string):string;

  end;
//Callback function
function CompareByScore(ls:TStringList;Index1,Index2:integer):integer;

var
  hsfrm: Thsfrm;

implementation

{$R *.dfm}

uses sh_fun;

{ Thsfrm }


//---------------------------------------------------------------------------
//Add an item on higscores ini file;
procedure Thsfrm.AddHighScore(Amolecule:string;
                              AScore:integer;
                              APlayerName:string);
var
  hs_file:TIniFile;
  i:integer;

begin
  ListView1.Items.Clear;
  i:=0;
  hs_file:=TIniFile.Create(HS_INI_FILENAME);

  //Ini file requires unique keys so, we have to use a dummy
  //removable symbol to distinguish among records made by the
  //same player
  while (hs_file.ValueExists(AMolecule,APlayerName+'#'+IntTostr(i))) do
  begin
    i:=i+1;
  end;

  hs_file.WriteString(AMolecule,APlayerName+'#'+IntTostr(i),IntToStr(AScore));
  hs_file.UpdateFile;
  hs_file.Free;

end;

//----------------------------------------------------------------------------
//It Checks if Player has a record score
function Thsfrm.IsRecord(AMolecule: string; Ascore:integer):boolean;
var
  i:integer;
  hs_file:TIniFile;
  highscores:TStringList;
  n:string;
  v:integer;

begin
  result:=TRUE;
  highscores:=TStringList.Create;
  hs_file:=TIniFile.Create(HS_INI_FILENAME);
  if hs_file.SectionExists(AMolecule) then
  begin
    hs_file.ReadSectionValues(AMolecule,highscores);

    for i := 0 to highscores.Count-1 do
    begin
      n:=highscores.Names[i];
      v:=StrToInt(Trim(highscores.Values[n]));
      if Ascore>=v then result:=FALSE;
    end;
  end;
  hs_file.UpdateFile;
  hs_file.Free;
  highscores.Free;
end;

//----------------------------------------------------------------------------
//(Re)Load ListView items from the ini high scores file for a given molecule
procedure Thsfrm.LoadHS(AMolecule: string);
var
  i:integer;
  hs_file:TIniFile;
  highscores:TStringList;
  NewItem:TListItem;
  n,temp:string;
  ind_pos:integer;

begin
  if (Amolecule='') then exit;

  highscores:=TStringList.Create;
  ListView1.Items.Clear;
  Caption:= 'High Scores for ' + AMolecule;
  hs_file:=TIniFile.Create(HS_INI_FILENAME);

  if hs_file.SectionExists(AMolecule) then
  begin
    hs_file.ReadSectionValues(AMolecule,highscores);

    highscores.CustomSort(CompareByScore);
    while highscores.Count>10 do
      highscores.Delete(highscores.Count-1);

    
    //We have to remove the dummy symbol (#n)
    //before loading in ListView
    
    for i := 0 to highscores.Count-1 do
    begin
      n:=highscores.Names[i];
      NewItem:=ListView1.Items.Add;
      NewItem.Caption:=n;
      ind_pos:=Pos('#',NewItem.Caption);
      temp:=NewItem.Caption;
      NewItem.Caption:=copy(temp,0,ind_pos-1);
      NewItem.SubItems.Add(highscores.Values[n])
    end;
  end;

  highscores.Free;
  hs_file.UpdateFile;
  hs_file.Free;
end;

//------------------------------------------------------------------------------
procedure Thsfrm.FormActivate(Sender: TObject);
begin
  OkBtn.SetFocus;
  

end;

//------------------------------------------------------------------------------
//CallBack function for custom sort in TStringList (take a look at the VCL help)
//It sorts records using moves value
function CompareByScore(ls: TStringList; Index1,
  Index2: integer): integer;
var
  n1,n2:string;
  v1,v2:integer;
begin
  Result:=-1;

  n1:=ls.Names[Index1];
  n2:=ls.Names[Index2];
  v1:=StrToInt(ls.Values[n1]);
  v2:=StrToInt(ls.Values[n2]);

  if v1 = v2 then Result:=0
  else
    if v1 > v2 then Result:=1;

end;

//------------------------------------------------------------------------------
//Get the record moves value for AMolecule level
function Thsfrm.GetRecord(AMolecule: string): string;
var
  hs_file:TIniFile;
  highscores:TStringList;
  n:string;

begin
  if (not FileExists(HS_INI_FILENAME)) or  (Amolecule='')
  then
  begin
    Result:='-';
    exit;
  end
  else
  begin
    hs_file:=TIniFile.Create(HS_INI_FILENAME);
    highscores:=TStringList.Create;

    if hs_file.SectionExists(AMolecule) then
    begin
      hs_file.ReadSectionValues(AMolecule,highscores);
      highscores.CustomSort(CompareByScore);
      n:=highscores.Names[0];
      Result:=highscores.Values[n];
    end
    else
      Result:='-';

    hs_file.Free;
    highscores.Free;
  end;
end;



end.
