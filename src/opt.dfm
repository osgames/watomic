object OptForm: TOptForm
  Left = 432
  Top = 202
  BorderStyle = bsDialog
  Caption = 'Application Settings'
  ClientHeight = 311
  ClientWidth = 218
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object OkBtn: TButton
    Left = 136
    Top = 288
    Width = 75
    Height = 17
    Caption = 'OK'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ModalResult = 1
    ParentFont = False
    TabOrder = 0
  end
  object WSizeRgp: TRadioGroup
    Left = 8
    Top = 112
    Width = 201
    Height = 89
    Hint = 
      'Select application main window size; if '#39'custom'#39' is selected the' +
      'n current window size will be mantained'
    Caption = 'Window size '
    ItemIndex = 1
    Items.Strings = (
      'for 800x600 resolution'
      'for 1024x768 resolution'
      'custom (save current size)')
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
  end
  object Panel1: TPanel
    Left = 8
    Top = 8
    Width = 201
    Height = 89
    TabOrder = 2
    object Label1: TLabel
      Left = 16
      Top = 8
      Width = 85
      Height = 13
      Caption = 'Animation'#39's speed'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Edit1: TEdit
      Left = 16
      Top = 28
      Width = 25
      Height = 21
      ReadOnly = True
      TabOrder = 0
      Text = '1'
      OnKeyPress = Edit1KeyPress
    end
    object TrackBar1: TTrackBar
      Left = 56
      Top = 26
      Width = 121
      Height = 25
      Hint = 'Select atom'#39's movement speed'
      Min = 1
      ParentShowHint = False
      Position = 1
      ShowHint = True
      TabOrder = 1
      OnChange = TrackBar1Change
    end
    object FrameSkCb: TCheckBox
      Left = 16
      Top = 64
      Width = 137
      Height = 17
      Hint = 'Avoid frame skipping but slow down atom'#39's motion'
      Caption = 'Prevent frame skipping'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
    end
  end
  object BrowserRgp: TRadioGroup
    Left = 8
    Top = 216
    Width = 201
    Height = 65
    Hint = 'Select the browser to view info pages with'
    Caption = 'Default browser for info pages'
    ItemIndex = 0
    Items.Strings = (
      'Internet Explorer'
      'Firefox')
    ParentShowHint = False
    ShowHint = True
    TabOrder = 3
  end
end
