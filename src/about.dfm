object AboutForm: TAboutForm
  Left = 355
  Top = 199
  BorderStyle = bsDialog
  Caption = 'About WAtomic 1.2.3'
  ClientHeight = 242
  ClientWidth = 323
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 96
    Top = 32
    Width = 148
    Height = 32
    Caption = 'Atomic  1.2'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -27
    Font.Name = 'Franklin Gothic Book'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 136
    Top = 96
    Width = 50
    Height = 12
    Caption = 'Thanks to:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -10
    Font.Name = 'Franklin Gothic Book'
    Font.Style = [fsBold, fsUnderline]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 8
    Top = 112
    Width = 309
    Height = 13
    Caption = 'Andreas W'#252'st for his KAtomic on wich is based this Windows clone'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -10
    Font.Name = 'Franklin Gothic Book'
    Font.Style = []
    ParentFont = False
  end
  object Label5: TLabel
    Left = 72
    Top = 128
    Width = 172
    Height = 13
    Caption = 'GLScene dev. team for its great work'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -10
    Font.Name = 'Franklin Gothic Book'
    Font.Style = []
    ParentFont = False
  end
  object Label6: TLabel
    Left = 232
    Top = 8
    Width = 84
    Height = 14
    Caption = 'ironfede (c) 2005'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Franklin Gothic Book'
    Font.Style = []
    ParentFont = False
  end
  object Label7: TLabel
    Left = 48
    Top = 80
    Width = 231
    Height = 13
    Caption = 'GPL Licensed, developed with Delphi + GlScene'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -10
    Font.Name = 'Franklin Gothic Book'
    Font.Style = []
    ParentFont = False
  end
  object Label8: TLabel
    Left = 88
    Top = 192
    Width = 156
    Height = 13
    Caption = 'J.K. and E.B. for testing WAtomic'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -10
    Font.Name = 'Franklin Gothic Book'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 96
    Top = 144
    Width = 126
    Height = 13
    Caption = 'Fabio C. for chemical infos'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -10
    Font.Name = 'Franklin Gothic Book'
    Font.Style = []
    ParentFont = False
  end
  object Label9: TLabel
    Left = 108
    Top = 160
    Width = 101
    Height = 13
    Caption = 'Lorenzo B. for sounds'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -10
    Font.Name = 'Franklin Gothic Book'
    Font.Style = []
    ParentFont = False
  end
  object Label10: TLabel
    Left = 48
    Top = 16
    Width = 42
    Height = 61
    Caption = 'W'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -48
    Font.Name = 'Franklin Gothic Medium'
    Font.Style = []
    ParentFont = False
  end
  object Label11: TLabel
    Left = 80
    Top = 176
    Width = 165
    Height = 13
    Caption = 'Mehdi Farhadi for testing and code'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -10
    Font.Name = 'Franklin Gothic Book'
    Font.Style = []
    ParentFont = False
  end
  object Button1: TButton
    Left = 256
    Top = 208
    Width = 49
    Height = 25
    Caption = 'OK'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ModalResult = 1
    ParentFont = False
    TabOrder = 0
  end
end
